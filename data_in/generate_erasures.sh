# !/bin/bash
echo $1
fichierOrigine=`basename $1 .log`
echo $fichierOrigine
fichierCible=$fichierOrigine"_ERASURES.log"
echo $fichierCible
nbLines=`wc -l < $1`
echo $nbLines
nbErasures=$(($nbLines/10))
echo $nbErasures
cp $1 $fichierCible
for i in $(seq 1 $nbErasures)
do
    line=$(( 1 + RANDOM %($nbLines - $i)))
    echo $line
    sed -i $line"d" $fichierCible
done

