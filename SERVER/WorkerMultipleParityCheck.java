package SERVER;


import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import DATA_STRUCTURES.ApplicationDataUnit;
import DATA_STRUCTURES.LoRaWANFrame;
import END_DEVICE.DeviceConfiguration;
import ENGINE.Decoder;




public class WorkerMultipleParityCheck implements Runnable {

	//Config decoder
	public DeviceConfiguration configuration;


	//Monitoring
	public int nbDataUnitDecoded;
	public double PDR;
	public double PER;
	public int nbErrorHash=0;

	boolean log;
	boolean trace;

	private Decoder decoder;
	
	//Buffer reception
	LinkedBlockingQueue<LoRaWANFrame> receptionBuffer; 


	public WorkerMultipleParityCheck(DeviceConfiguration DC, boolean trace) {
		this.configuration = DC;
		this.trace = trace;
		this.nbDataUnitDecoded = 0;
		this.PDR=0.0;
		this.PER=1.0;
		this.receptionBuffer = new LinkedBlockingQueue<LoRaWANFrame>();
	}

	public Decoder getDecoder() {
		return this.decoder;
	}

	public int exec() {

		//Monitoring
		int maxDataUnitDecoded = 1;
		int nbLoRaWANFramesReceived = 0;
		int maxLoRaWANFramesReceived = 0;
		
		int firstLoRaWANReceived = 0;
		boolean firstTime = true;
		
		try {

			Vector<ApplicationDataUnit> rcvApplicativeFrms ;

			this.decoder = new Decoder(this.configuration.WL, this.configuration.APL);

//			System.out.println(this.decoder);
			
			if (trace)System.out.println("Start Reception DeviceID["+this.configuration.ID
					+"]. APL["+ this.configuration.APL
					+"] WL["+this.configuration.WL+"].");

			while (true) {
				//				System.out.println("Wait 4 new fragments.");
				LoRaWANFrame receivedFrame = this.receptionBuffer.take();
				//				System.out.println("Received2 new fragments.");
//				System.out.println(receivedFrame);
				if(firstTime) {
					firstLoRaWANReceived = receivedFrame.frameCounter;
					firstTime = false;		
				}
				
				//EXIT /!\ Trick pour demo 
				if (receivedFrame.payload.length == 0) {
					System.out.println("Received marker end");
					return nbDataUnitDecoded;
				}

				//				if (trace) System.out.println("Receive LoRaWAN Frame:"+receivedFrame);
				nbLoRaWANFramesReceived++;

				
				if( maxLoRaWANFramesReceived<=receivedFrame.frameCounter - firstLoRaWANReceived) {
					maxLoRaWANFramesReceived = (receivedFrame.frameCounter - firstLoRaWANReceived)+1;
				}

//				System.out.println("\nnbLoRaWANFramesReceived="+nbLoRaWANFramesReceived);
//				System.out.println("maxLoRaWANFramesReceived="+maxLoRaWANFramesReceived);
//				System.out.println("receivedFrame.frameCounter="+receivedFrame.frameCounter);
//				System.out.println("firstLoRaWANReceived="+firstLoRaWANReceived);
				
//				System.out.println("\nnbLoRaWANFrameseceived="+nbLoRaWANFramesReceived+" maxLoRaWANFramesReceived="+maxLoRaWANFramesReceived);
				this.PER = 1.0 - ((double) nbLoRaWANFramesReceived / (double) maxLoRaWANFramesReceived) ;
//				System.out.println("this.PER=" + this.PER);

				rcvApplicativeFrms = decoder.receiveFrame(receivedFrame);
				if (rcvApplicativeFrms == null) {
					//Error Hash
					nbErrorHash++;
					rcvApplicativeFrms = decoder.receiveFrame(receivedFrame);
				}
				if (rcvApplicativeFrms == null) {
					System.out.println("Incoherent data received (MIC false)");
					System.out.println(this.decoder);
				}else {


					for (int i=0; i<rcvApplicativeFrms.size(); i++) {
						nbDataUnitDecoded++;
						// TODO decoded frames doivent être envoyé à la couche supérieure d'une maniere ou d'une autre.
						//					if(trace) System.out.println(rcvApplicativeFrms.elementAt(i));
						if(rcvApplicativeFrms.elementAt(i).getApplicationFrameCounter()+1>maxDataUnitDecoded) {
							maxDataUnitDecoded=rcvApplicativeFrms.elementAt(i).getApplicationFrameCounter()+1;
						}
					}
				}
				this.PDR = (double)nbDataUnitDecoded/(double)maxDataUnitDecoded;
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public 	LinkedBlockingQueue<LoRaWANFrame> getReceptionBuffer(){
		return this.receptionBuffer;
	}



	@Override
	public void run() {
		this.exec();
	}



}
