package SERVER;

class RunningDecoder {
	Thread thread;
	WorkerMultipleParityCheck workerMultipleParityCheck;
	String devId;

	public RunningDecoder(Thread t, WorkerMultipleParityCheck s, String id) {
		this.thread = t;
		this.workerMultipleParityCheck = s;
		this.devId = id;
	}
}