package SERVER;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import DATA_STRUCTURES.LoRaWANFrame;

public class Logger {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\\u001B[37m";
	
	public static final String ANSI_BLACK_BACKGROUND = "\\u001B[40m";
	public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
	public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
	public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
	public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
	public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
	public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
	
	
	private boolean TRACE_MINIMAL = false;
	private boolean TRACE_ERASURE_CORRECTOR = false;
	private boolean TRACE_DEFRAGMENTER = false;
	private boolean TRACE_INTEGRITY = false;
	private boolean TRACE_ERASURE_CORRECTOR_lite = false;
	private boolean TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX = false;
	private boolean TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS = false;
	private boolean TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS = false;
	private boolean TRACE_FRAGMENT_PARSER = true;

	private boolean LOG = false;
//	public static final boolean SHOW_RESOLVER = true;
//	public static final boolean SHOW_DEFRAGMENTER = true;
//	public static final boolean SHOW_INTEGRITY = true;


	
	public Logger() {
		this.setLOG(false);
		boolean traceNothing = false;
		boolean traceGeneralBehavior = false ;
		boolean traceDetails = false;
		boolean traceAll = false;
		
		if (traceNothing) { 
			TRACE_MINIMAL = false;
			TRACE_ERASURE_CORRECTOR = false;
			TRACE_DEFRAGMENTER = false;
			TRACE_INTEGRITY = false;
			TRACE_ERASURE_CORRECTOR_lite = false;
			TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX = false;
			TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS = false;
			TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS = false;
		}
		if (traceGeneralBehavior) {
			TRACE_MINIMAL = true;
			TRACE_ERASURE_CORRECTOR = false;
			TRACE_DEFRAGMENTER = false;
			TRACE_INTEGRITY = false;
			TRACE_ERASURE_CORRECTOR_lite = false;
			TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX = false;
			TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS = false;
			TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS = false;
		}
		if (traceDetails) { 
			TRACE_MINIMAL = true;
			TRACE_ERASURE_CORRECTOR = true;
			TRACE_DEFRAGMENTER = true;
			TRACE_INTEGRITY = true;
			TRACE_ERASURE_CORRECTOR_lite = false;
			TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX = false;
			TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS = true;
			TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS = false;
		}
		if (traceAll) { 
			TRACE_MINIMAL = true;
			TRACE_ERASURE_CORRECTOR = true;
			TRACE_DEFRAGMENTER = true;
			TRACE_INTEGRITY = true;
			TRACE_ERASURE_CORRECTOR_lite = false;
			TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX = false;
			TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS = true;
			TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS = false;
		}
		
	}
	
	public void traceFragmentParser(String S) {
		if (this.isTRACE_FRAGMENT_PARSER()) {
			System.out.print( S );
		}
	}
	
	public void traceErasureCorrector(String S) {
		if (this.resolverEnabled()) {
			System.out.print( S );
		}
	}
	
	public void traceErasureCorrectorLite(String S) {
		if (TRACE_ERASURE_CORRECTOR_lite || this.resolverEnabled()) {
			System.out.print( S );
		}
	}
	
	public void traceDefragmenter(String S) {
		if (this.defragmenterEnabled()) {
			System.out.print( S );
		}
	}
	
	public void traceIntegrity(String S) {
		if (this.integrityEnabled()) {
			System.out.print( S );
		}
	}
	
	public void trace(String S) {
		if (TRACE_MINIMAL) {
			System.out.print( S );
		}
	}
	
	public void traceLite(String S) {
		if (this.integrityEnabled() || this.defragmenterEnabled() || this.resolverEnabled() || TRACE_ERASURE_CORRECTOR_lite ) {
			System.out.print( S );
		}
	}

	public void traceErasureCorrectorDiagonalise(String S) {
		if (TRACE_ERASURE_CORRECTOR_DIAGONALISE_MATRIX) {
			System.out.print( S );
		}
	}
	public void traceErasureCorrectorDataManagerDetails(String S) {
		if (TRACE_ERASURE_CORRECTOR_DATA_MANAGER_DETAILS) {
			System.out.print( S );
		}
	}

	public void traceErasureCorrectorDeliverFragments(String S) {
		if (TRACE_ERASURE_CORRECTOR_DELIVER_FRAGMENTS) {
			System.out.print( S );
		}
	}

	
	/**
	 * @return the tRACE_RESOLVER
	 */
	public boolean resolverEnabled() {
		return TRACE_ERASURE_CORRECTOR;
	}

	/**
	 * @param tRACE_RESOLVER the tRACE_RESOLVER to set
	 */
	public void setTRACE_RESOLVER(boolean tRACE_RESOLVER) {
		TRACE_ERASURE_CORRECTOR = tRACE_RESOLVER;
	}

	/**
	 * @return the tRACE_DEFRAGMENTER
	 */
	public boolean defragmenterEnabled() {
		return TRACE_DEFRAGMENTER;
	}

	/**
	 * @param tRACE_DEFRAGMENTER the tRACE_DEFRAGMENTER to set
	 */
	public void setTRACE_DEFRAGMENTER(boolean tRACE_DEFRAGMENTER) {
		TRACE_DEFRAGMENTER = tRACE_DEFRAGMENTER;
	}

	/**
	 * @return the tRACE_INTEGRITY
	 */
	public boolean integrityEnabled() {
		return TRACE_INTEGRITY;
	}

	/**
	 * @param tRACE_INTEGRITY the tRACE_INTEGRITY to set
	 */
	public void setTRACE_INTEGRITY(boolean tRACE_INTEGRITY) {
		TRACE_INTEGRITY = tRACE_INTEGRITY;
	}

	/**
	 * @return the lOG
	 */
	public boolean islOG() {
		return LOG;
	}

	/**
	 * @param lOG the lOG to set
	 */
	public  void setLOG(boolean LOG) {
		this.LOG = LOG;
	}

	/**
	 * @return the tRACE_FRAGMENT_PARSER
	 */
	public boolean isTRACE_FRAGMENT_PARSER() {
		return TRACE_FRAGMENT_PARSER;
	}

	/**
	 * @param tRACE_FRAGMENT_PARSER the tRACE_FRAGMENT_PARSER to set
	 */
	public void setTRACE_FRAGMENT_PARSER(boolean tRACE_FRAGMENT_PARSER) {
		TRACE_FRAGMENT_PARSER = tRACE_FRAGMENT_PARSER;
	}
	
	
	

}
