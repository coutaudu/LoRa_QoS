package SERVER;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.LinkedList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import DATA_STRUCTURES.LoRaWANFrame;

public class FRONT_END_SERVER implements Runnable {

	public LinkedList<RunningDecoder> runningDecoders;
	int port;

	/**
	 * @param runningDecoders
	 */
	public FRONT_END_SERVER(int port) {
		super();
		this.runningDecoders = new LinkedList<RunningDecoder>();
		this.port = port;
	}

	@Override
	public void run() {
		try {
			// System.out.println("-- Start Server --");
			HttpServer server;
			server = HttpServer.create(new InetSocketAddress(this.port), 0);
			server.createContext("/test", new httpHandler(this));
			server.setExecutor(null); // creates a default executor
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static class httpHandler implements HttpHandler {
		FRONT_END_SERVER frontEndServer;

		/**
		 * @param frontEndServer
		 */
		public httpHandler(FRONT_END_SERVER frontEndServer) {
			super();
			this.frontEndServer = frontEndServer;
		}

		@Override
		public void handle(HttpExchange t) throws IOException {
			try {
//				System.out.println("Received something");
				String httpPost = Utility.getStringFromInputStream(t.getRequestBody());
//				System.out.println(httpPost);
				LoRaWANFrame rcvFrame = parseReceivedTextJSON(httpPost);
//				System.out.println("Parsed to a abstract lorawan frame");
				// Decode Frame
				Integer deviceIndex = this.frontEndServer.deviceIsRegistred(rcvFrame);
				if (deviceIndex != -1) {
//					System.out.println("Device Registred");
					int size = this.frontEndServer.runningDecoders.get(deviceIndex).workerMultipleParityCheck.receptionBuffer.size();
//					System.out.println(size);

					this.frontEndServer.runningDecoders.get(deviceIndex).workerMultipleParityCheck.receptionBuffer.put(rcvFrame);
//					System.out.println("Rcv frame pushed.");
				} else {
//					System.out.println("Received Frame from Unregistred Device.");
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			String response = "This is the response";
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();

		}
	}

	//TODO doit etre implémzntanté avec une Map !!
	private Integer deviceIsRegistred(LoRaWANFrame rcvFrame) {
		for (int i = 0; i < runningDecoders.size(); i++) {
			if (runningDecoders.get(i).devId.compareTo(rcvFrame.deviceID) == 0) {
				return i;
			}
		}
		return -1;
	}

	public void pushWorker(Thread thread, WorkerMultipleParityCheck serverMultipleParityCheck, String id) {
		runningDecoders.add(new RunningDecoder(thread, serverMultipleParityCheck, id));
	}

	public static class RunningDecoder {
		public Thread thread;
		public WorkerMultipleParityCheck workerMultipleParityCheck;
		String devId;

		public RunningDecoder(Thread t, WorkerMultipleParityCheck s, String id) {
			this.thread = t;
			this.workerMultipleParityCheck = s;
			this.devId = id;
		}
	}

	public static LoRaWANFrame parseReceivedTextJSON(String S){
	 
		try {

			String LoRaQoSDevID;
			Integer LoRaWANCounter;

			JSONParser parser = new JSONParser();
			Object obj;
			obj = parser.parse(S);
	
			JSONObject jsonObject = (JSONObject)obj;
			
			String hardware_serial = jsonObject.get("hardware_serial").toString();
			String port = jsonObject.get("port").toString()	;
			LoRaQoSDevID = port + "_" + hardware_serial;
//			System.out.println("LoRaQoSDevID["+LoRaQoSDevID+"]");
	
			LoRaWANCounter = Integer.parseInt(jsonObject.get("counter").toString());
//			System.out.println("LoRaWANCounter["+LoRaWANCounter+"]"); 
	
			Object obj2 = parser.parse(jsonObject.get("payload_raw").toString());
			JSONObject jsonPayload = (JSONObject)obj2;
			String payload = jsonPayload.get("data").toString();
//			System.out.println("Payload["+payload+"]"); 
			
			JSONArray jsonPayloadArray = (JSONArray) parser.parse(payload);

			Integer[] loraqosRawBytes = new Integer[jsonPayloadArray.size()];
			for (int i=0; i<jsonPayloadArray.size(); i++) {
				loraqosRawBytes[i] = Integer.parseInt (jsonPayloadArray.get(i).toString());
//				System.out.print("."+rawBytesReceived[i-1]);
			}
//			System.out.println("\n-----");
			LoRaWANFrame rcvFrame = new LoRaWANFrame(LoRaQoSDevID, LoRaWANCounter, loraqosRawBytes);
			
			return rcvFrame;
					
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
