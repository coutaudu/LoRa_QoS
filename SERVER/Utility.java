package SERVER;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import DATA_STRUCTURES.LoRaWANFrame;

public class Utility {

	
	
	public Utility() {
		// TODO Auto-generated constructor stub
	}
	

	public static String BooleanTableToString(Boolean[] table) {
		String S = "";
		for (int i=0; i<table.length; i++) {
			if(table[i].booleanValue()) {
				S+="1";
			}else{
				S+="0";
			}
		}
		return S;
	}
	
	public static String IntegerTableToHexaString(Integer[] table) {
		String S = "";
		for (int i=0; i<table.length; i++) {
			S += String.format("%3X", table[i]);
		}
		return S;
	}
	
	public static void kbstroke() {
		System.out.println("-- PRESS ENTER --");
				try {
					System.in.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
	}

	public static Integer[] convertRcvPacket(DatagramPacket packet, byte[] buffer){
		Integer[] frameReceived;
		frameReceived = new Integer[packet.getLength()];
		for (int i=0; i<packet.getLength(); i++) {
			int uvalue = (int) (buffer[i] & 0xFF);
			frameReceived[i] = uvalue;
		}
		return frameReceived;
	}
	
	
	
	// convert InputStream to String
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append('\n');
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
	
	public static boolean isLocalPortInUse(int port) {
		try {
			// ServerSocket try to open a LOCAL port
			new DatagramSocket(port).close();
			// local port can be opened, it's available
			return false;
		} catch(IOException e) {
			// local port cannot be opened, it's in use
			return true;
		}
	}
	
	public static Integer parseID(Integer[] data) {
		return new Integer(data[0]);
		
	}

	public static Integer parseFrameNumber(Integer[] data) {
		Integer frmNum = new Integer(data[1]);
		frmNum += 256 * data[2];
		frmNum += 256 * 256 * data[3];
		frmNum += 256 * 256 * data[4];
		return frmNum;
	}

}
