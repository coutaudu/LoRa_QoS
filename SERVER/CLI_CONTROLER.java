package SERVER;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;

import org.junit.experimental.theories.Theories;

import END_DEVICE.DeviceConfiguration;
import END_DEVICE.DeviceSimulator;
import SERVER.FRONT_END_SERVER.RunningDecoder;
import Tests_Demos.DEMO;
import Tests_Demos.STATS;
import Tests_Demos.UDPtoHTTPadaptor;

//import com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverFragment;


/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class CLI_CONTROLER {




	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) {

		// Démarre le serveur qui reçoit les données depuis le réseaux.
		FRONT_END_SERVER frontEndServer = new FRONT_END_SERVER(8000);
		UDPtoHTTPadaptor udptohttp = new UDPtoHTTPadaptor(8000, "http://localhost:8000/test");
		Thread frontEndServerThread = new Thread(frontEndServer);
		Thread udptohttpThread = new Thread(udptohttp);

		frontEndServerThread.start();
		udptohttpThread.start();

		DeviceConfiguration DC = null; 

		int userChoice = 0;

		while (true) {
			displayCommandPrompt();
			userChoice = getUserCommand();

			switch (userChoice) {

			case 1:
				DC = configureDeviceManually();
				launchDecoder(DC, frontEndServer.runningDecoders);
				break;

			case 2:  
				DC = configureFromFile();
				if (DC!=null) launchDecoder(DC, frontEndServer.runningDecoders);
				break;

			case 3:
				System.out.println("\t[1] Configure from file.");
				System.out.println("\t[2] Configure Manually.");
				if (DC != null) {
					System.out.println("\t[3] Last configuration [APL:"+DC.APL+";WL:"+DC.WL+";ID:"+DC.ID+"]");
				}
				int uc = getUserCommand();
				if (uc==1) DC = configureFromFile();
				else if (uc==2) DC = configureDeviceManually();
				else if (uc!=3)  DC = null;
				launchDeviceSimulation(DC, frontEndServer.port);
				break;

			case 4: 
				monitor(frontEndServer.runningDecoders);
				break;

			case 5: 
				System.out.println("Choose decoder to terminate:");
				int rd = userSelectDecoder(frontEndServer);
				if (rd!=-1) {
					frontEndServer.runningDecoders.remove(rd);
				}
				break;

			case 6: 
				DeviceConfiguration devParam = configureFromFile();
				if (devParam != null) {
					System.out.println("Choose appCnt to begin with :\n");
					int AppCnt = getUserCommand();
					devParam.PostConfigurationToDevice(AppCnt);
				}
				break;

			case 7: 
				boolean exitMonitoring = false;
				while(!exitMonitoring) {
					monitor(frontEndServer.runningDecoders);
					try {
						Thread.sleep(1000);
						if (System.in.available()>0) {
							exitMonitoring = true;
						}
					} catch (InterruptedException | IOException e) {
						e.printStackTrace();
					}
				}
				break;

			case 8:
				System.out.println("Choose connexion to manage");
				monitor(frontEndServer.runningDecoders);
				int choiceWorker = getUserCommand();
				System.out.println("Choice=" +choiceWorker);
				if (choiceWorker<frontEndServer.runningDecoders.size()) {
					Logger currentLogger = frontEndServer.runningDecoders.get(choiceWorker).workerMultipleParityCheck.getDecoder().getDataManager().getLogger();
					if (currentLogger.resolverEnabled()) {
						System.out.println("[0] Disable Erasure Correction Trace.");
					}else {
						System.out.println("[0] Enable Erasure Correction Trace.");
					}
					if (currentLogger.defragmenterEnabled()) {
						System.out.println("[1] Disable Defragmenter Trace.");
					}else {
						System.out.println("[1] Enable Defragmenter Trace.");
					}
					if (currentLogger.integrityEnabled()) {
						System.out.println("[2] Disable Integrity Trace.");
					}else {
						System.out.println("[2] Enable Integrity Trace.");
					}
					int choiceAction = getUserCommand();
					if (choiceAction==0) currentLogger.setTRACE_RESOLVER(!currentLogger.resolverEnabled());
					else if (choiceAction==1) currentLogger.setTRACE_DEFRAGMENTER(!currentLogger.defragmenterEnabled());
					else if (choiceAction==2) currentLogger.setTRACE_INTEGRITY(!currentLogger.integrityEnabled());
				}
				break;

			case 9: 
				System.out.println("[EXIT]");
				System.exit(0);
				break;
			default	:
				break;
			}

			System.out.println("\n\n\n");
		}

	}



	private static String runningDecoderToString(RunningDecoder RD) {
		WorkerMultipleParityCheck SMPC = RD.workerMultipleParityCheck;
		String status = null;
		boolean isAlive = RD.thread.isAlive();
		if (isAlive) {
			status = "Running";
		}else {
			status = "Finished";
		}

		String StringPDR = String.format("%.2f", (SMPC.PDR));
		String StringPER = String.format("%.2f", (SMPC.PER));

		return "Decoder[APL="+SMPC.configuration.APL
				+";WL="+SMPC.configuration.WL
				+"; DevID="+SMPC.configuration.ID
				+"] Rcv:"+ SMPC.nbDataUnitDecoded 
				+ " PDR:" + StringPDR  
				+ " PER:" + StringPER 
				+ " --- "  + status ;
	}

	private static void monitor(LinkedList<RunningDecoder> runningDecoders) {
		System.out.println("["+runningDecoders.size()+"] decoders running currently.");	
		for (int i=0; i<runningDecoders.size(); i++) {
			System.out.println("("+i+")"+runningDecoderToString(runningDecoders.get(i)));
		}
		for (int i=0; i<runningDecoders.size(); i++) {
			boolean isAlive = runningDecoders.get(i).thread.isAlive();
			if (!isAlive) {
				runningDecoders.remove(i);
				i=0;
			}
		}		
	}


	private static void launchDecoder(DeviceConfiguration dC, LinkedList<RunningDecoder> runningDecoders) {
		if (dC != null) {
			WorkerMultipleParityCheck S = new WorkerMultipleParityCheck(dC, true);
			Thread T = new Thread( S );
			RunningDecoder RD = new RunningDecoder(T, S, dC.ID);
			runningDecoders.add(RD);
			T.start();
		}		
	}


	private static void launchDeviceSimulation(DeviceConfiguration dC, int port) {
		if (dC != null) {
			System.out.println("How many frames ?");
			int nbFramesSend = getUserCommand();
			System.out.println("Packet Error Rate ? (%)");
			double PER = (double)getUserCommand()/100.0;
			try {
				new DeviceSimulator(dC, port, nbFramesSend, PER);
			} catch (IOException e) {
				System.out.println("Failed to create device simulation process.");	
				e.printStackTrace();
			}
		}		
	}


	private static DeviceConfiguration configureFromFile() {
		DeviceConfiguration DC = null;
		File dir = new File ("./configurations");
		String[] strs = dir.list();
		System.out.println("\tFile configuration:");
		for (int i = 0; i < strs.length; i++) {
			System.out.println ("\t["+i+"] "+ strs[i]);
		}
		int userChoice =  getUserCommand();
		if (userChoice<0 || userChoice>strs.length-1) {
			return null;
		}else{
			System.out.println ("\t\t\t["+userChoice+"] "+ strs[userChoice]);
			try {
				int APL;
				int WL;
				String ID;
				FileReader configFile = new FileReader("./configurations/"+strs[userChoice]);
				BufferedReader BR = new BufferedReader(configFile);
				APL = Integer.parseInt(BR.readLine());
				WL = Integer.parseInt(BR.readLine());
				ID = BR.readLine();
				System.out.println(" Configuration:");
				System.out.println(" APL  - " + APL);
				System.out.println(" WL   - " + WL);
				System.out.println(" ID - " + ID);
				DC = new DeviceConfiguration(APL, WL, ID);
				BR.close();
			} catch (FileNotFoundException e1) {
				System.out.println("Error reading configuration file.");
				e1.printStackTrace();
			} catch (IOException e) {
				System.out.println("Error reading configuration file.");
				e.printStackTrace();
			}
			return DC;
		}
	}

	private static int userSelectDecoder(FRONT_END_SERVER frontEndServer) {
		monitor(frontEndServer.runningDecoders);
		int choiceWorker = getUserCommand();
		System.out.println("Choice=" +choiceWorker);
		if (choiceWorker<frontEndServer.runningDecoders.size()) {
			return choiceWorker;
		} else {
			return -1;
		}
		
		
	}
	


	private static DeviceConfiguration configureDeviceManually() {
		DeviceConfiguration DC = null;
		int APL = 0;
		int WL = 0;
		String id  = "0";

		System.out.println("Manual configuration:");
		//		System.out.println("\tDevice ID ?");
		//		System.out.println("(Not implemented yet)");
		System.out.print("\tApplicative Payload Length ? ");

		APL =  getUserCommand();
		if (APL < 1 ) {
			System.out.println("Invalid Applicative Payload Length. Abort.");
			return null ;
		}

		System.out.print("\tWindow Length ? ");
		WL = getUserCommand();
		if (WL < 1 ) {
			System.out.println("Invalid Window Length. Abort.");
			return null;
		}

		System.out.print("\tDevice ID ? ");
		id = Integer.toString(getUserCommand());

		DC = new DeviceConfiguration(APL, WL, id);
		return DC;		
	}


	private static void  displayCommandPrompt() {
		System.out.println("[1] Configure new receptor manually");
		System.out.println("[2] Configure new receptor from file");
		System.out.println("[3] Launch Device Simulation");
		System.out.println("[4] List running decoder");
		System.out.println("[5] Remove Decoder");
		System.out.println("[6] Modify Device Over The Air");
		System.out.println("[7] Monitor");
		System.out.println("[8] Trace settings");
		System.out.println("[9] Exit");
		System.out.println("[0] Cancel");
	}

	private static int getUserCommand() {
		String keyboardInput = null;
		int userChoice = 0;
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

		try {
			keyboardInput = bufferRead.readLine();
			userChoice = Integer.parseInt(keyboardInput);

		} catch (IOException e1) {
			userChoice = 0;
		} catch (NumberFormatException e2) {
			userChoice = 0;
		}

		return userChoice;
	}	


}



