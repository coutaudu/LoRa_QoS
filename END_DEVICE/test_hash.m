%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function RES  = test_hash(FRAME, AppCntMSB)

      bits15_8 = bitshift(bitand(AppCntMSB,bitshift(255,0)),0);
      bits23_16 = bitshift(bitand(AppCntMSB,bitshift(255,8)),-8);
      bits31_24 = bitshift(bitand(AppCntMSB,bitshift(255,16)),-16);

      FULL_TRAME = [ bits31_24 bits23_16 bits15_8  FRAME(1:end-2)];
      
      Computed_H = hash("sha256", char(FULL_TRAME))(1:4);
      CH = [ hex2dec(Computed_H(1:2)) hex2dec(Computed_H(3:4)) ];
      RCV_HASH = FRAME(end-1:end);

      RES = (CH == RCV_HASH);
endfunction
