%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [RECOVERED_DATA FRAGMENTS_BUFFER] = defragmentation_MPC(FRAGMENTS_BUFFER,  DECODED_FRAGMENTS)

  global TRACE;
  global NB_FRAG;
  global CDUL;
  
  RECOVERED_DATA = [];
 
  total_paquet_buffer = floor(rows(FRAGMENTS_BUFFER)/NB_FRAG);
  for ii = 1:total_paquet_buffer
    count = 0;
    for jj = 1:NB_FRAG
      ind = ((ii-1)*NB_FRAG) + jj;
      if any(FRAGMENTS_BUFFER(ind,:))
	count++;
      endif
    endfor
    
    if count==NB_FRAG
      FRAME = [];
      for jj = 1:NB_FRAG
	ind = ((ii-1)*NB_FRAG) + jj;
	FRAME = [ FRAME FRAGMENTS_BUFFER(ind,2:end)];
	FRAGMENTS_BUFFER(ind,:)=zeros(1,columns(FRAGMENTS_BUFFER)); #Sors la trame du buffer
      endfor
      RECOVERED_DATA = [ RECOVERED_DATA; FRAME(1:CDUL) ];
    endif
  endfor
endfunction
