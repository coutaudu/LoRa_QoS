%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				% Encoder Multiple Parity Check

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###  SENDER  ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function CODED_FRAMES = sender_MPC(DATA_UNITS, WL, FL)
  global TRACE;
  global SIMU_RESET;
  global CDUL;  
#  NB_FRM_EXP = rows(DATA_UNITS)*2; #TODO * 2  avec le coding rate 1/2 
  FRAGMENTS_UNITS = fragmentation_MPC(DATA_UNITS, FL);

  
  NB_FRAGS_BY_DATA_UNITS = ceil(CDUL / FL);

  NB_FRG = rows(FRAGMENTS_UNITS);
  ENCODED_FRAMES = encoder_MPC (FRAGMENTS_UNITS, WL, NB_FRAGS_BY_DATA_UNITS);

  if 0==1 #Ajoute FRM_COUNTER
    for ii = 1:rows(ENCODED_FRAMES)
      CODED_FRAMES(ii,:) = [(ii-1), ENCODED_FRAMES(ii,:)]; #Ajoute FRM_COUNTER
    endfor
  else
      CODED_FRAMES = ENCODED_FRAMES; #Ajoute FRM_COUNTER
  endif
  NB_CODED_FRAG = rows(CODED_FRAMES);
  if TRACE
    CODED_FRAMES
  endif
  
endfunction
