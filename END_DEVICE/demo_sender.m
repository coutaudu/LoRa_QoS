%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Multiple Parity Check

## USAGE:
## octave-cli MPC.m WinLength NbPackets PayloadLength PER
pkg load instrument-control
cd ./src/END_DEVICE/
clear
FALSE = (0==1);
TRUE = (0==0);
##global TRACE = TRUE;
global TRACE = FALSE;
global PAUSE = FALSE;
VERBOSE = TRUE;
TEST_AUTO = FALSE;

global OVERLAP = FALSE;
global SIMU_RESET = FALSE;
global INDEX_SIMU_RESET;
global ERASE_RANGE = FALSE;
global ERASE_B_MIN;
global ERASE_B_MAX;
TEST_ECC_PERF = FALSE;

global NB_RESET = 0;
global NB_PAQUETS_ABANDONNES = 0;

source("./routines.m")

global FL = 1;
global APL= 1;
global CDUL= 1;

global LAT;
LAT = 0;

				#global current_recept = 0;
global TOTAL_LAT = 0;

if rows(argv()) >= 6
  DEV_ID               = str2num(argv(){1});
  udp_port             = str2num(argv(){2});
  global WL            = str2num(argv(){3});
  NB_FRAMES_EXPERIMENT = str2num(argv(){4});
  APL                  = str2num(argv(){5});
  PER                  = str2num(argv(){6});
  if rows(argv()) >= 7
    disp("OPT detected")
    disp(argv(){6})
    if strcmp(argv(){7},"OVERLAP")
      if VERBOSE
	disp("SET OVERLAP");
      endif
      OVERLAP = TRUE;

    elseif strcmp(argv(){7},"ERASE_RANGE")
      ERASE_RANGE = TRUE;
      ERASE_B_MIN = str2num(argv(){8});
      ERASE_B_Max = str2num(argv(){9});

    elseif strcmp(argv(){7},"TEST_ECC_PERF")
      TEST_ECC_PERF = TRUE;

    elseif strcmp(argv(){7},"SIMU_RESET")
      SIMU_RESET = TRUE;
      INDEX_SIMU_RESET = str2num(argv(){8});
    else
      disp("ERROR COMMAND LINE ARGUMENT");
    endif
  endif
else
  NB_WIN = 10;
  global WL            = 16;
  NB_FRAMES_EXPERIMENT = WL * NB_WIN;
  APL                  = 20; # Applicative_Payload_Length
  PER                  = 0.3;
endif
APP_CNT_LENGTH       = 1;
HASH_LENGTH          = 2;
CDUL          = APL + APP_CNT_LENGTH + HASH_LENGTH; # Code_Data_Unit_Length

## compute_fragment_length TODO Error ici
MIN_AVAILABLE_PAYLOAD = 10;
A = ceil(CDUL/(MIN_AVAILABLE_PAYLOAD-1));
FL = ceil(CDUL/A);
if VERBOSE
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  PER
  NB_FRAMES_EXPERIMENT
  WL
  FL
  APL
  NB_FRAGS_BY_DATA_UNITS = ceil(CDUL / FL)
  PADDING =  (NB_FRAGS_BY_DATA_UNITS * FL) - CDUL
endif
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###   MAIN   ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
##ans = input("Press any key to continue");

%-------------------------------------------------------------------------------------------%
#                       ##############
#                       ### SENDER ###
#                       ##############
%-------------------------------------------------------------------------------------------%
if SIMU_RESET
  DATA_UNITS1 = generate_data_units(INDEX_SIMU_RESET,APL,0); # compute AppCnt, hash
  FRAMES1 = sender_MPC(DATA_UNITS1, WL, FL); #decoupe l'appPayLoad, compute la redondance le frag    
  DATA_UNITS2 = generate_data_units(NB_FRAMES_EXPERIMENT-INDEX_SIMU_RESET,APL,12);
  FRAMES2 = sender_MPC(DATA_UNITS2, WL, FL);

  FRAMES = [ FRAMES1 ; FRAMES2 ];
  
  if VERBOSE
    disp(['[', num2str(rows(FRAMES)), '] fragments sent. (with RST).']);
  endif
  
else
  DATA_UNITS = generate_data_units(NB_FRAMES_EXPERIMENT,APL,0); # compute AppCnt, hash
  if VERBOSE
    disp(['[', num2str(rows(DATA_UNITS)), '] Application Data Units generated.']);
  endif

  FRAMES = sender_MPC(DATA_UNITS, WL, FL); #decoupe l'appPayLoad, compute la redondance le frag_cnt,x colle les fragments ensemble pour remplir le paquet. 

  if VERBOSE
    disp(['[', num2str(rows(FRAMES)), '] fragments sent.']);
  endif
endif

		     #TODO gather MPC_frames in physical larger frames

## Ajoute le frmae counter de lorawan avant l'emission des trames
## Ajoute egalement le DEV_ID
#  DEV_ID=1;
if (TRUE)
  for ii_frame_counter = 1:rows(FRAMES)
    NumerotedFRAMES(ii_frame_counter,:) = [DEV_ID typecast(uint32(ii_frame_counter-1),"uint8") FRAMES(ii_frame_counter,:)  ];
  endfor
  FRAMES =  NumerotedFRAMES;
endif


%-------------------------------------------------------------------------------------------%
#                       ###############
#                       ### CHANNEL ###
#                       ###############
%-------------------------------------------------------------------------------------------%

NB_FRAGMENTS_SENT = rows (FRAMES);
FRAMES = channel_MPC(FRAMES, PER);
				#FRAMES
if TRACE
  FRAMES
  head('');
  if PAUSE
    ans = input("Press any key to continue");
    ##    pause;
  endif
endif

NB_FRAGMENTS_RCV = rows (FRAMES);
NB_FRAGMENTS_LOST = NB_FRAGMENTS_SENT - NB_FRAGMENTS_RCV;
EFFECTIVE_PER = (NB_FRAGMENTS_LOST / NB_FRAGMENTS_SENT) *100;
if VERBOSE
  disp(['[', num2str((EFFECTIVE_PER)), '] effective per.']);
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
endif


				%-----------------%
				# ############### #
				# ### CHANNEL ### #
				# ############### #
				%-----------------%


disp("Create UDP");
destinationPort=udp_port;
r = rand();
r = int32(r*1000);
sourcePort=udp_port+r;
UDP_SEND = udp("127.0.0.1", destinationPort, sourcePort, 100);


pause(1);
				#DATA = input("Type:","s")

disp("Send");

				#format("hex")
tic;
for ii = 1:1:rows(FRAMES)
%  pause(1);
  if (mod(ii,20)==19)
    ii;
    time_to_pause = (0.04*((NB_FRAGS_BY_DATA_UNITS)*(1+((WL/35)^5))))/(NB_FRAGS_BY_DATA_UNITS*2); % Experimental approximation
 %   pause(time_to_pause*20);
  endif

				%typecast(FRAMES(ii,:),"uint8")
  DATA = uint8(FRAMES(ii,:));
  N = udp_write(UDP_SEND,DATA);
				%  SEND = [];

endfor
elapsed_time = toc
exit = [DEV_ID typecast(uint32(ii_frame_counter-1),"uint8")]
exit = uint8(exit)

N = udp_write(UDP_SEND,exit);


disp("Close UDP");

udp_close(UDP_SEND)


