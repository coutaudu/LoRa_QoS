%returns nextprbs value with seed start
function r=prbs23(start)
x= start;
b0 = bitand(x,1);
b1 = bitand(x,32)/32;
x = floor(x/2) + bitxor(b0,b1)*2^22;
r=x;


