%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function code = encoder_MPC (FRAGMENTS_UNITS, WL, NB_FRAGS_BY_DATA_UNITS)
  global TRACE;
  global FL ;
  NB_FRM_EXP = rows(FRAGMENTS_UNITS)*2; #TODO * 2  avec le coding rate 1/2 
#  FRAGMENTS_UNITS = fragmentation_MPC(DATA_UNITS, FL);
  DATA_UNITS = FRAGMENTS_UNITS;
  ## ##########################################################################
  ## CODAGE ###################################################################
  ## ##########################################################################
  systematic_fcnt = 0;
  redundancy_fcnt = 0;

  FRAG_NUMBER_MAX = 128 - ( mod(128 , NB_FRAGS_BY_DATA_UNITS ))

  for (i_frm = 1:NB_FRM_EXP)
    if mod(i_frm,2)==1
      %% SYSTEMATIC
      FRAMES(i_frm,1) = mod(systematic_fcnt,FRAG_NUMBER_MAX); ##Trames systematiques sont numerotées de 0 à 127
      for jj = 1: columns(DATA_UNITS) # Remplis avec le App Data Unit
	FRAMES(i_frm,jj+1) = DATA_UNITS(systematic_fcnt+1,jj);
      endfor
      systematic_fcnt++;
    else
%%      disp ('>>>>------------------------------------');
  %%    i_frm
      %% REDUNDANT
      FRAMES(i_frm,1) = mod(redundancy_fcnt,FRAG_NUMBER_MAX)+128; ## Les trames de code sontnumerotées de 128 à 255
      FRAMES(i_frm,2) = 0;
   %%   redundancy_fcnt
   %%   systematic_fcnt
      mat_line = mod (systematic_fcnt-1, FRAG_NUMBER_MAX);
      RC = get_random_combinaison(WL,mat_line);
      
      MAT_GEN(systematic_fcnt,:) = RC; ## Uniquement pour pouvoir observer la matrice generatrice de la parité; inutile du point de vue fonctionnel.
      ##disp('---');
      for (j_red = 1:columns(RC))
	if (RC(j_red) == 1)
	  ind_red = (systematic_fcnt + (j_red - WL));
	  if (ind_red > 0)
	    ##disp([ 'i_frm[',num2str(i_frm),'] j_red[', num2str(j_red), '] ind_red[', num2str(ind_red), ']' ]);
	      if( FRAMES(i_frm,1) == 151 )
	%%	ind_red
		DATA_UNITS(ind_red,:);
	      endif
	    for jj = 1:columns(DATA_UNITS)
	      FRAMES(i_frm,jj+1) = bitxor(FRAMES(i_frm,jj+1), DATA_UNITS(ind_red,jj) );
	    endfor
	  endif
	endif
      endfor
      FRAMES(i_frm,:);
      redundancy_fcnt++;
%%      disp ('------------------------------------<<<<');
    endif
  endfor
  code = FRAMES;

  if TRACE
    MAT_GEN
  endif

endfunction
