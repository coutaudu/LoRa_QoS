%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [DELIVERED_DATA NB_ERRORS] = decodeur_MPC(FRAMES, WL, FL,  CDUL)

  global TRACE;
  global PAUSE;
  FALSE = (0==1);
  TRUE = (0==0);
  DELIVERED_DATA = [];
  global NB_FRAG;

  MAX_APP_COUNT_LSB = 255;
  NB_ERRORS = 0;

######################
### DATA STRUCTURE ###
######################
  global RESOLVE_MATRIX;
  global FRAGMENTS_MATRIX;
  global FLAGS_DELIVERY;
  global last_treated;
  global FRAG_COUNT;
  global FRAGMENTS_BUFFER;
  global FRAMES_COUNTERS;

  global APP_COUNT;
  global CURRENT_RECEPT;

##############
### SERVER ###
##############

  initialize_decoder_MPC(WL, FL);
  AppCntMSB = 0;

  for i_frm = 1:rows(FRAMES)
    FC = parser(FRAMES(i_frm,:),"FRAME_COUNTER");
    ##    if mod(FC,4)==0
    FC = floor(FC/(2*NB_FRAG));
    ##    endif
    
    ##----------##
    ## DECODAGE ##
    ##----------##
    resolver_MPC( FRAMES(i_frm,:));
    ##-----------------##
    ## DEFRAGMENTATION ##
    ##-----------------##
    [RECOVERED_DATA FRAGMENTS_BUFFER] = defragmentation_MPC(FRAGMENTS_BUFFER);
    
    if TRACE && !(isempty(RECOVERED_DATA))
      RECOVERED_DATA
      if PAUSE
	ans = input("Press any key to continue");
	##pause;
      endif
    endif
    
    ##----------------##
    ##   INTEGRITE    ##
    ##----------------##
    [NEW_DELIVERED_DATA ERRORS AppCntMSB] = integrity(RECOVERED_DATA, AppCntMSB, WL);
    DELIVERED_DATA = [DELIVERED_DATA; NEW_DELIVERED_DATA];

    
    ##    APP_COUNTER = FRAMES_COUNTERS(end)/2
    ##    current = i_frm/2
    if ERRORS>0
      lastval=FRAMES_COUNTERS(end);
      for ii = rows(FRAMES_COUNTERS)-1:-1:1
	if FRAMES_COUNTERS(ii) != 0
	  diff = lastval - FRAMES_COUNTERS(ii);
	  if diff > 255
	    i_frm;
	    gobackto = 31-ii;
	    break;
	  endif
	endif
      endfor

#      if (exist("gobackto","var")==1)
	initialize_decoder_MPC(WL, FL);
	## TODO -> received_frames_buffer
	i_frm = i_frm - gobackto;
#      endif
    endif

    if !(isempty(NEW_DELIVERED_DATA))
				#      current_ADU = (i_frm-1)/2
				#      NEW_DELIVERED_DATA
				#      disp('---');
      ##      LAT += 
      #      disp('^^^');
    endif
    
    NB_ERRORS += ERRORS;
    if TRACE && !(isempty(NEW_DELIVERED_DATA))
      DELIVERED_DATA = NEW_DELIVERED_DATA
      if PAUSE
	##pause;
	ans = input("Press any key to continue");
      endif
    endif

  endfor

endfunction
