%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [RES_RCR RES_RDR] = simplify_receive_combination(RCR, RDR, RM, DM)

  RESOLVE_MATRIX = RM;
  DATA_MATRIX = DM;
  
      # TRAITE LA DONNEES RECU DANS LA COMBINAISON ALEATOIRE DE LA TRAME
    for ii = 1:columns(RCR)
      if RCR(ii) == 1 ## La trame ii est contenue dans la combinaison

	#infom(['Track[',num2str(ii),']']);
	if RESOLVE_MATRIX(2*ii-1,ii) == 1 ## Déjà isolé ? auquel cas je peux l'éliminer
			      #  infom(['Found[', num2str(ii), ']' ]);
	  RCR = bitxor(RESOLVE_MATRIX(2*ii-1,:), RCR);
	  RDR = bitxor(DATA_MATRIX(2*ii-1,:), RDR);
	endif

	if RESOLVE_MATRIX(2*ii,ii) == 1 ## Déjà reçu ? Je peux éliminer de cette combinaison.
	#  infom(['Found2[', num2str(ii), ']' ]);
 	  RCR = bitxor(RESOLVE_MATRIX(2*ii,:), RCR);
 	  RDR = bitxor(DATA_MATRIX(2*ii,:), RDR);
	endif
	
      endif
    endfor

    RES_RCR = RCR;
    RES_RDR = RDR;

endfunction
