%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Multiple Parity Check

## USAGE:
## octave-cli MPC.m WinLength NbPackets PayloadLength PER

clear
FALSE = (0==1);
TRUE = (0==0);
##global TRACE = TRUE;
global TRACE = FALSE;
global PAUSE = FALSE;
VERBOSE = FALSE;
TEST_AUTO = TRUE;

global OVERLAP = FALSE;
global SIMU_RESET = FALSE;
global INDEX_SIMU_RESET;
global ERASE_RANGE = FALSE;
global ERASE_B_MIN;
global ERASE_B_MAX;
TEST_ECC_PERF = FALSE;

global NB_RESET = 0;
global NB_PAQUETS_ABANDONNES = 0;

source("./routines.m")

global FL = 1;
global APL= 1;
global CDUL= 1;

global LAT;
LAT = 0;

#global current_recept = 0;
global TOTAL_LAT = 0;

if rows(argv()) >= 4
  if VERBOSE
    disp('Arguments from command line:');
  endif
  global WL            = str2num(argv(){1});
  NB_FRAMES_EXPERIMENT = str2num(argv(){2});
  APL                  = str2num(argv(){3});
  PER                  = str2num(argv(){4});
  if rows(argv()) >= 5

    if strcmp(argv(){5},"OVERLAP")
      if VERBOSE
	disp("SET OVERLAP");
      endif
      OVERLAP = TRUE;

    elseif strcmp(argv(){5},"ERASE_RANGE")
      ERASE_RANGE = TRUE;
      ERASE_B_MIN = str2num(argv(){6});
      ERASE_B_Max = str2num(argv(){7});

    elseif strcmp(argv(){5},"TEST_ECC_PERF")
      TEST_ECC_PERF = TRUE;

    elseif strcmp(argv(){5},"SIMU_RESET")
      SIMU_RESET = TRUE;
      INDEX_SIMU_RESET = str2num(argv(){6});
    else
      disp("ERROR COMMAND LINE ARGUMENT");
    endif
  endif
else
  NB_WIN = 25;
  global WL            = 16;
  NB_FRAMES_EXPERIMENT = WL * NB_WIN;
  APL                  = 8; # Applicative_Payload_Length
  PER                  = 0.60;
endif
  APP_CNT_LENGTH       = 1;
  HASH_LENGTH          = 2;
  CDUL          = APL + APP_CNT_LENGTH + HASH_LENGTH; # Code_Data_Unit_Length

  ## compute_fragment_length
  FL = ceil(APL/ (ceil((CDUL)/10)))+3;
  if VERBOSE
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    PER
    NB_FRAMES_EXPERIMENT
    WL
    FL
    APL
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  endif
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###   MAIN   ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ##ans = input("Press any key to continue");

%-------------------------------------------------------------------------------------------%
#                       ##############
#                       ### SENDER ###
#                       ##############
%-------------------------------------------------------------------------------------------%
  if SIMU_RESET
    DATA_UNITS1 = generate_data_units(INDEX_SIMU_RESET,APL,0); # compute AppCnt, hash
    FRAMES1 = sender_MPC(DATA_UNITS1, WL, FL); #decoupe l'appPayLoad, compute la redondance le frag    
    DATA_UNITS2 = generate_data_units(NB_FRAMES_EXPERIMENT-INDEX_SIMU_RESET,APL,12);
    FRAMES2 = sender_MPC(DATA_UNITS2, WL, FL);

    FRAMES = [ FRAMES1 ; FRAMES2 ];

    if VERBOSE
      disp(['	[', num2str(rows(FRAMES)), '] fragments sent. (with RST).']);
    endif
    
  else
    DATA_UNITS = generate_data_units(NB_FRAMES_EXPERIMENT,APL,0); # compute AppCnt, hash
    if VERBOSE
      disp(['	[', num2str(rows(DATA_UNITS)), '] Application Data Units generated.']);
    endif

    FRAMES = sender_MPC(DATA_UNITS, WL, FL); #decoupe l'appPayLoad, compute la redondance le frag_cnt,x colle les fragments ensemble pour remplir le paquet. 

    if VERBOSE
      disp(['	[', num2str(rows(FRAMES)), '] fragments sent.']);
    endif
  endif
%-------------------------------------------------------------------------------------------%
#                       ###############
#                       ### CHANNEL ###
#                       ###############
%-------------------------------------------------------------------------------------------%

NB_FRAGMENTS_SENT = rows (FRAMES);
FRAMES = channel_MPC(FRAMES, PER);

if TRACE
  FRAMES
  head('');
  if PAUSE
  ans = input("Press any key to continue");
##    pause;
  endif
endif

NB_FRAGMENTS_RCV = rows (FRAMES);
NB_FRAGMENTS_LOST = NB_FRAGMENTS_SENT - NB_FRAGMENTS_RCV;
EFFECTIVE_PER = (NB_FRAGMENTS_LOST / NB_FRAGMENTS_SENT) *100;
if VERBOSE
  disp(['	[', num2str((EFFECTIVE_PER)), '] effective per.']);
endif
%-------------------------------------------------------------------------------------------%
%---------------------------------##################----------------------------------------%
%-------------------------------### SERVER DECODER ###--------------------------------------%
%---------------------------------##################----------------------------------------%
%-------------------------------------------------------------------------------------------%

global NB_FRAG = ceil(CDUL / FL);

[DELIVERED_FRAGMENTS NB_ERROR] = decodeur_MPC(FRAMES, WL, FL, CDUL);




#########
## LOG ##
#########
NB_APP_DATA_UNIT_DELIVERED = rows(DELIVERED_FRAGMENTS);
NB_PAQUETS_ABANDONNES = NB_FRAMES_EXPERIMENT - NB_APP_DATA_UNIT_DELIVERED;
APPLICATIVE_PER = (NB_PAQUETS_ABANDONNES / NB_FRAMES_EXPERIMENT);


if VERBOSE
  disp(['	[', num2str(rows(FRAMES)), '] fragments received .']);
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  NB_APP_DATA_UNIT_DELIVERED
  NB_PAQUETS_ABANDONNES
  NB_ERROR
  APPLICATIVE_PER  
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  
endif

	   
RES_PDR = [ WL EFFECTIVE_PER 1-APPLICATIVE_PER ];
filename = ['./LOG/RES_PDR_WL', num2str(WL),'_APL',num2str(APL),'_NBADU', num2str(NB_FRAMES_EXPERIMENT), '.log'];
csvwrite(filename,RES_PDR,"-append");


RES_LAT = [ WL EFFECTIVE_PER TOTAL_LAT/NB_FRAMES_EXPERIMENT ];

filename = ['./LOG/RES_LAT_WL', num2str(WL),'_APL',num2str(APL),'_NBADU', num2str(NB_FRAMES_EXPERIMENT), '.log'];
csvwrite(filename,RES_LAT,"-append");

if TEST_AUTO
  if TEST_ECC_PERF

    if RES_PDR(3)>0.95
      disp(">95%");
    elseif RES_PDR(3)>0.80
      disp(">80%");
    elseif RES_PDR(3)>0.50
      disp(">50%");
    elseif (1-RES_PDR(2)/100+0.1)>RES_PDR(3) && RES_PDR(3)>(1-RES_PDR(2)/100-0.1)
      disp("PDR~PER");
    endif

  else
    
    if NB_ERROR == 0
      disp("NOERROR")
    endif
    if NB_RESET == 2 && NB_ERROR ==1 # La mise en route + 1 fois une desynchro des buffers
      disp("RESET");
    endif
    
  endif
endif
