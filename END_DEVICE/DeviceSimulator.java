package END_DEVICE;

import java.io.IOException;

public class DeviceSimulator {

	Process P;

	/**
	 * @param aPL
	 * @param wL
	 * @param nbFramesSend
	 * @param port
	 * @throws IOException 
	 */
	public DeviceSimulator(DeviceConfiguration DC, int port, int nbFramesSend, double PER) throws IOException {
		
		Runtime runtime = Runtime.getRuntime();

		String callOctave;
		
		if (System.getProperty("os.name").startsWith("Windows")) {
			callOctave = "C:\\Users\\ucoutaud\\Octave-4.4.0\\bin\\octave-cli.exe" + " C:\\Users\\ucoutaud\\eclipse-workspace\\LoRaQoS\\src\\END_DEVICE\\demo_sender.m ";
		}else {
			callOctave = "octave-cli" + " /home/coutaudu/eclipse-workspace/LoRaQoS/src/END_DEVICE/demo_sender.m " ;
		}
		
		this.P = runtime.exec( callOctave
				+ DC.ID
				+" " + port
				+" "+ DC.WL //winLength
				+" "+ nbFramesSend
				+" " + DC.APL // Applicative Payload Length
				+" " + PER); //PER
	}

}