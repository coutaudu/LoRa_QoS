package END_DEVICE;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

//import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

public class DeviceConfiguration{
	public int APL;
	public int WL;
	public String ID;

	public DeviceConfiguration(int aPL, int wL, String iD) {
		this.APL = aPL;
		this.WL = wL;
		this.ID = iD;
	}

	public void PostConfigurationToDevice(int AppCnt) {
		this.PostConfigurationToDevice(AppCnt, "PostConfig");
	}

	public void PostConfigurationToDevice(int AppCnt, String target) {
		URL url;
		try {
			url = new URL("http://localhost:20000/"+target);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); // PUT is another valid option
			http.setDoOutput(true);
	
			Map<String,String> arguments = new HashMap<>();
			byte[] dataB = new byte[6];

			String DataS = new String(dataB);
			DataS = "02";
			DataS += String.format("%02X",this.APL);
			DataS += String.format("%02X",this.WL);
			DataS += String.format("%04X",AppCnt);
			arguments.put("msg=", DataS);

			StringJoiner sj = new StringJoiner("&");
			for(Map.Entry<String,String> entry : arguments.entrySet()) {
				sj.add(entry.getKey()+entry.getValue());
			}
			byte[] out = sj.toString().getBytes();
			int length = out.length;
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			http.connect();

			try(OutputStream os = http.getOutputStream()) {
				os.write(out);
			}
			http.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
