
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
####################### ################ #######################
####################### ###  SENDER  ### #######################
####################### ################ #######################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


## TODO
# ajouter les nouveaux en-têtes
#    AppCnt
#    hash
# calculer taille des fragments
# decouper en fragments
# ajouter les entetes de fragments
# construire la trame a envoyer 
function retval = generate_data_units(NB_FRM_EXP, APL, valStart)
  global TRACE;
  AppCntLSB = 0;
  AppCnt = 0;
  HL = 2; #Hash Length

  count_data_gen = valStart;
  for i = 1:NB_FRM_EXP
    bits7_0 = bitshift(bitand(AppCnt,bitshift(255,0)),0);
    bits15_8 = bitshift(bitand(AppCnt,bitshift(255,8)),-8);
    bits23_16 = bitshift(bitand(AppCnt,bitshift(255,16)),-16);
    bits31_24 = bitshift(bitand(AppCnt,bitshift(255,24)),-24);
    AppCntLSB = bits7_0;
    DATA(i,1) = AppCntLSB;

    appPayloadStart=1+1;
    appPayloadEnd=APL+1;
    for j = appPayloadStart:appPayloadEnd % moidifed for no frame counter
      DATA(i,j) = ('A') + mod(count_data_gen++,26);
    endfor
    

    HASH_SPAN = [ bits31_24 bits23_16 bits15_8 bits7_0 DATA(i,appPayloadStart:appPayloadEnd)];
    HASH256 = hash("sha256",char(HASH_SPAN));
    H1 = hex2dec(HASH256(1:2));
    H2 = hex2dec(HASH256(3:4));
    DATA(i,appPayloadEnd+1) = H1; %% modified for no frame counter
    DATA(i,appPayloadEnd+2) = H2;
    AppCnt++;
  endfor
  retval = DATA;
endfunction
