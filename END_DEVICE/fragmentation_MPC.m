%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COUTAUD Ulysse                        %
% SEMTECH                               %
% Universite Grenoble Alpes             %
% ulysse.coutaud@univ-grenoble-alpes.fr %
% ucoutaud@semtech.com                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Todo ename CDUL
function FRAGMENTS_UNITS = fragmentation_MPC( DATA_UNITS, FL)

  NB_FRM_EXP = rows(DATA_UNITS)*2; #TODO * 2  avec le coding rate 1/2 
  global CDUL ;
  ##
  ## FRAGMENTE ##############################################################
  ##
  yy = 1;
  NB_FRAG = ceil(CDUL / FL);
  PADDING =  (NB_FRAG * FL) - CDUL;

  for ii = 1:rows(DATA_UNITS)
    for jj = 1: NB_FRAG
      borne_inf = (jj-1) * FL + 1;
      borne_max = min((jj) * FL,columns(DATA_UNITS));
      ll = 0;
      for kk = borne_inf:borne_max
	ll++;
	FRAGMENTS_UNITS(yy,ll) = DATA_UNITS(ii,kk);
     endfor
      yy++;
    endfor
  endfor
  ## #############################################################################


endfunction
