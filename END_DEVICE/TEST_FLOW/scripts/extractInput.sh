#! /bin/bash


if [ $# -ne 3 ]
then
	echo "Usage: ./extractInput.sh devHWEUI fileSrc repertoryDst"
	return
fi

devID=$1
fileSrc=$2
repertoryDst=$3

writeInput=false
while read LINE
do
	if [[ "$LINE" == \>\>\>\>\>\>\>*\<\<\<\<\<\<\< ]]
	then
		#echo $LINE

		APL=$(echo $LINE | cut -f1 -d,)
		APL=$(echo $APL | cut -f2 -d[)

		WL=$(echo $LINE | cut -f2 -d,)
		WL=$(echo $WL | cut -f1 -d])
		
		#echo "APL="$APL
		#echo "WL="$WL
		fileDestination=$repertoryDst"/ID-"$devID"-APL-"$APL"-WL-"$WL-".input"
		rm -f $fileDestination
		writeInput=true
		read LINE #La 1ere ligne correspond au uplink de la configuration precedente (necessaire poiur envoyer le downlink)
	elif $writeInput
	then
		echo $LINE >> $fileDestination
	fi
done < "$fileSrc"
	