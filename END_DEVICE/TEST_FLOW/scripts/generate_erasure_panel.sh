# !/bin/bash


if [ $# -ne 2 ]
then
	echo "Usage: ./generate_erasure_panel.sh repertorySrc repertoryDst"
	return
fi

repertorySrc=$1
repertoryDst=$2
nbFiles=`ls $repertorySrc | wc -l`
echo $nbFiles
nbDone=0
for file in `ls $repertorySrc`
do
	for PER in `seq 0 10 20`
	do
		echo "Start "$repertorySrc"/"$file" "$PER
		./scripts/generate_erasures.sh $repertorySrc"/"$file $repertoryDst $PER &
	done
	nbDone=$(($nbDone+1))
	wait
	echo "["$nbDone"/"$nbFiles"]"	
done
