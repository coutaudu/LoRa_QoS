# !/bin/bash

if [ $# -ne 3 ]
then
	echo "Usage: ./generate_erasures.sh fileSrc repertoryDst percentageErasure"
	return
fi

fileSrc=$1
repertoryDst=$2
percentageErasure=$3


#echo "Generate "$2"% erasures over input flow: "$1
fichierOrigine=`basename $1 .input`
#echo $fichierOrigine
fichierCible=$repertoryDst"/"$fichierOrigine"PER-"$percentageErasure"-.input"

#echo "Results in file: "$fichierCible
nbLines=`wc -l < $fileSrc`
#echo "Source has "$nbLines" lines."
nbErasures=$(($nbLines*$percentageErasure))
nbErasures=$(($nbErasures/100))
#echo "Erase "$nbErasures" lines."
cp $fileSrc $fichierCible

for i in $(seq 1 $nbErasures)
do
    line=$(( 1 + RANDOM %($nbLines - $i)))
    #echo "Erase line: "$line
    sed -i $line"d" $fichierCible
done

echo "{\"app_id\":\"196222540965505\",\"dev_id\":\"track\",\"hardware_serial\":\"70B3D57ED000FF50\",\"port\":201,\"counter\":10000,\"payload_raw\":{\"type\":\"Buffer\",\"data\":[]},\"metadata\":{\"time\":\"2018-11-02T09:49:38.356682182Z\",\"frequency\":868.5,\"modulation\":\"LORA\",\"data_rate\":\"SF7BW125\",\"airtime\":71936000,\"coding_rate\":\"4/5\",\"gateways\":[{\"gtw_id\":\"eui-7276ff0039030023\",\"timestamp\":1052732740,\"time\":\"\",\"channel\":7,\"rssi\":-65,\"snr\":7.8,\"rf_chain\":1,\"latitude\":45.209152,\"longitude\":5.7888136,\"location_source\":\"registry\"}]},\"payload\":{\"type\":\"Buffer\",\"data\":[64]},\"_msgid\":\"e3b462d2.978aa\"}" >> $fichierCible
echo "{\"app_id\":\"196222540965505\",\"dev_id\":\"track\",\"hardware_serial\":\"70B3D57ED000FF51\",\"port\":201,\"counter\":10000,\"payload_raw\":{\"type\":\"Buffer\",\"data\":[]},\"metadata\":{\"time\":\"2018-11-02T09:49:38.356682182Z\",\"frequency\":868.5,\"modulation\":\"LORA\",\"data_rate\":\"SF7BW125\",\"airtime\":71936000,\"coding_rate\":\"4/5\",\"gateways\":[{\"gtw_id\":\"eui-7276ff0039030023\",\"timestamp\":1052732740,\"time\":\"\",\"channel\":7,\"rssi\":-65,\"snr\":7.8,\"rf_chain\":1,\"latitude\":45.209152,\"longitude\":5.7888136,\"location_source\":\"registry\"}]},\"payload\":{\"type\":\"Buffer\",\"data\":[64]},\"_msgid\":\"e3b462d2.978aa\"}" >> $fichierCible
