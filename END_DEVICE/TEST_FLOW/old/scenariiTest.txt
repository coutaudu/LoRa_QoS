#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#% COUTAUD Ulysse                        %
#% SEMTECH                               %
#% Universite Grenoble Alpes             %
#% ulysse.coutaud@univ-grenoble-alpes.fr %
#% ucoutaud@semtech.com                  %
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Usage: octave-cli MPC.m windowLength   nbFramesAppli   appliPayloadLength   PER  

[1] No counter overflow. No fragmentation.
octave-cli MPC.m 8 64 7 0.2  > /tmp/RES.log

[2] Counter overflow. No fragmentation.
octave-cli MPC.m 8 555 7 0.2  > /tmp/RES.log

[3] No Counter overflow. Fragmentation without padding.
octave-cli MPC.m 8 64 17 0.2  > /tmp/RES.log

[4] No Counter overflow. Fragmentation with padding.
octave-cli MPC.m 8 64 15 0.2  > /tmp/RES.log

[5] Counter overflow. Fragmentation with padding.
octave-cli MPC.m 8 300 15 0.2  > /tmp/RES.log

[6] Counter overflow with overlap. No fragmentation.
octave-cli MPC.m 8 300 7 0.2 OVERLAP > /tmp/RES.log

[7] Test(1) Erasure Correction Performances.
octave-cli MPC.m 8 300 7 0.1 TEST_ECC_PERF > /tmp/RES.log

[8] Test(2) Erasure Correction Performances.
octave-cli MPC.m 16 500 7 0.4 TEST_ECC_PERF > /tmp/RES.log

[9] Test(3) Erasure Correction Performances.
octave-cli MPC.m 16 500 7 0.7 TEST_ECC_PERF > /tmp/RES.log

[10] RESET 1.
octave-cli MPC.m 16 500 7 0.1 SIMU_RESET 100 > /tmp/RES.log

[11] RESET 2 .
octave-cli MPC.m 16 500 20 0.1 SIMU_RESET 300 > /tmp/RES.log

[12] RESET 3.
octave-cli MPC.m 16 500 20 0.1 SIMU_RESET rand1 SIMU_RESET rand2 SIMU_RESET rand3 > /tmp/RES.log

[13] Test toute taille payload applicative et toute PER.
for windowLength = 8, 16, 32
                for PER = 1:2:99
                                for taille_payload = 1:255
                                                octave-cli MPC.m 16 500 taille_payload PER > /tmp/RES.log
                                endfor
                endfor
endfor
