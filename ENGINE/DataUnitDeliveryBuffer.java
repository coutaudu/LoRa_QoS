package ENGINE;
import java.util.Arrays;
import java.util.LinkedList;

import DATA_STRUCTURES.BytesMatrix;
import DATA_STRUCTURES.DataManager;
import DATA_STRUCTURES.LogicalMatrix;
import SERVER.Logger;
import SERVER.Utility;

/**
 * 
 */

/**
 * Objet g�rant le transfert des donn�es entre la couche de erasure correction
 * et la couche de defragmentation
 * @author ucoutaud
 *
 */
public class DataUnitDeliveryBuffer {

	private int NB_MAX_FRAGMENTS_IN_BUFFER; //!
	private LogicalMatrix statusDeliveredFromPreviousLayer; // Flag signalant que cette entr�e contient un fragment "clair"
	private LogicalMatrix statusDeliveredToNextLayer; // Flag signalant que cette entr�e � �t� r�cup�r� par la couche d�frag
	private BytesMatrix resolvedDataUnits; // Le buffer contenant les fragments en transit.
	private Long absoluteFragmentCounter; // Compteur permettant de situer les fragments re�us dans le flow
	private DataManager dataManager;
	
	public DataUnitDeliveryBuffer(DataManager DM) {
		this.setCounter(new Long(-1)); 
		this.dataManager=DM;
		this.NB_MAX_FRAGMENTS_IN_BUFFER = this.dataManager.DECODING_DEPTH*this.dataManager.getWindowSize();
		this.statusDeliveredFromPreviousLayer = new LogicalMatrix(NB_MAX_FRAGMENTS_IN_BUFFER, 1, true); // init a true pour ne pas faire remonter 
		this.statusDeliveredToNextLayer = new LogicalMatrix(NB_MAX_FRAGMENTS_IN_BUFFER, 1, true); // les fragments de "avant le debut"
//		this.resolvedDataUnits = new BytesMatrix(NB_MAX_FRAGMENTS_IN_BUFFER, fragmentsPayloadLength);
	}

	
	public void receiveFromCorrectionLayer(LogicalMatrix resolvedCombinationMatrix, BytesMatrix resolvedDataMatrix, int fragmentNumberMax ) {
		this.resolvedDataUnits = resolvedDataMatrix;
		for (int i=0; i<resolvedCombinationMatrix.nbRows()/2; i++) {
			if(resolvedCombinationMatrix.rowWithSingleOne(i)) {
				this.statusDeliveredFromPreviousLayer.setElement(i, 0, true);;
			}
		}
	}

	
	public int getBufferLength() {
		return NB_MAX_FRAGMENTS_IN_BUFFER;
	}
	
	/**
	 * @return the statusDeliveredFromCorrectionLayer
	 */
	protected LogicalMatrix getStatusDeliveredFromPreviousLayer() {
		return statusDeliveredFromPreviousLayer;
	}

	/**
	 * @param statusDeliveredFromCorrectionLayer the statusDeliveredFromCorrectionLayer to set
	 */
	public void setStatusDeliveredFromPreviousLayer(LogicalMatrix statusDeliveredFromCorrectionLayer) {
		this.statusDeliveredFromPreviousLayer = statusDeliveredFromCorrectionLayer;
	}

	/**
	 * @return the statusDeliveredToDefragmentationLayer
	 */
	public LogicalMatrix getStatusDeliveredToNextLayer() {
		return statusDeliveredToNextLayer;
	}

	/**
	 * @param statusDeliveredToDefragmentationLayer the statusDeliveredToDefragmentationLayer to set
	 */
	public void setStatusDeliveredToNextLayer(LogicalMatrix statusDeliveredToDefragmentationLayer) {
		this.statusDeliveredToNextLayer = statusDeliveredToDefragmentationLayer;
	}

	/**
	 * @return the resolvedFragments
	 */
	public BytesMatrix getResolvedDataUnits() {
		return resolvedDataUnits;
	}

	/**
	 * @param resolvedDataUnits the resolvedFragments to set
	 */
	public void setResolvedDataUnits(BytesMatrix resolvedDataUnits) {
		this.resolvedDataUnits = resolvedDataUnits;
	}

	/**
	 * @return the fragmentCounter
	 */
	public Long getCounter() {
		return absoluteFragmentCounter;
	}

	/**
	 * @param fragmentCounter the fragmentCounter to set
	 */
	public void setCounter(Long fragmentCounter) {
		this.absoluteFragmentCounter = fragmentCounter;
	}

	public void shift() {
		this.statusDeliveredFromPreviousLayer.shiftUp();
		this.statusDeliveredToNextLayer.shiftUp();
//		this.resolvedDataUnits.shiftUp();
		this.setCounter(this.getCounter()+1);
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = new String();
		S += "fragN In | Data ";
		for (int j=0; j<this.resolvedDataUnits.nbColumns()-1;j++) {
			S += "   ";
		}
		S += "| Out\n";


		for (int i=0; i<this.statusDeliveredToNextLayer.nbRows(); i++) {
			
			if (this.statusDeliveredFromPreviousLayer.getElement(i, 0) 
					&& !this.statusDeliveredToNextLayer.getElement(i, 0) ) {
				S += String.format("  %3d ", i);

				S += this.statusDeliveredFromPreviousLayer.elementToString(i, 0) + " | ";
				for (int j=0; j<this.resolvedDataUnits.nbColumns();j++) {
					S += this.resolvedDataUnits.elementToString(i, j);
				}
				S += " | " + this.statusDeliveredToNextLayer.elementToString(i, 0)+ "\n";
			}
		}
		return S;
	}
	
	
		
}
