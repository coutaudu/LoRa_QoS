package ENGINE;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

import DATA_STRUCTURES.BytesMatrix;
import DATA_STRUCTURES.DataManager;
import DATA_STRUCTURES.Fragment;
import DATA_STRUCTURES.LogicalMatrix;
import SERVER.Utility;
import javafx.util.Pair;

/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class ErasureCorrector {

	/* ******************* *
	 *     ATTRIBUTES      *
	 * ******************* */

	private DataManager dataManager;

	private LogicalMatrix resolvedCombinationMatrix;
	private BytesMatrix resolvedDataMatrix;

	private int expectedFragmentNumber;


	/* ******************* *
	 *     CONSTRUCTOR     *
	 * ******************* */

	/**
	 * @param dataResolveMatrix
	 * @param combinationResolveMatrix
	 * @param fragmentDeliveryStatus
	 * @param lastFragmentNumberTreated
	 * @param expectedFragmentNumber
	 * @param windowSize
	 * @param fragmentLength
	 */
	public ErasureCorrector(DataManager DM) {

		this.setDataManager(DM);

		this.setExpectedFragmentNumber(this.computeExpectedFragmentNumber());

		this.resolvedCombinationMatrix = new LogicalMatrix(1, 1);
		this.resolvedDataMatrix = new BytesMatrix(1, 1);

		// A l'initialisation, on d�marre avec matrice connue (Trame du "pass�e" sont implicite)... Ou alors on rattrape les premieres combinaisons et t supprime les "1" incoh�rents.
		this.resetCombinationResolveMatrix();
	}

	public void resetCombinationResolveMatrix() {
		int i_max=this.getWindowSize()*this.getDataManager().DECODING_DEPTH;
		for (int i=0; i<i_max; i++) {
			this.getCombinationResolveMatrix().setElement(i*2, i, Boolean.TRUE); // *2 correspond au fait qu'on a un CR 1/2
		}
	}

	/* ******************* *
	 *       METHODS       *
	 * ******************* */
	/* 
	 * TODO doit retourner un ensemble de fragments 
	 *(>>>*AVEC*<<< le numero de fragment pour defragmenter)
	 */
	public void pushFragment(Fragment F) {
		
		this.getDataManager().getLogger().traceErasureCorrector("\tInsert fragment into correction matrix.\n");	
		this.insert(F); 
		this.getDataManager().getLogger().traceErasureCorrector("\tDiagonalise correction matrix.\n");
		this.diagonalise();
		this.getDataManager().getLogger().traceErasureCorrector("\tDeliver new resolved fragments.\n");
		this.getFragmentsDeliveryBuffer().receiveFromCorrectionLayer(this.getResolvedCombinationMatrix(), this.getResolvedDataMatrix(), this.getDataManager().getFragmentNumberMax());

	}


	private void diagonalise() {
		this.getCombinationResolveMatrix().copy(this.getResolvedCombinationMatrix());
		this.getDataResolveMatrix().copy(this.getResolvedDataMatrix());
		boolean frameFirstOccurrenceAlreadyFound = false;
		for (int i_col = 0; i_col<this.getWindowSize()*this.getDataManager().DECODING_DEPTH; i_col++) {
			frameFirstOccurrenceAlreadyFound = false;
			for (int i_lig=i_col; i_lig<this.getWindowSize()*2*this.getDataManager().DECODING_DEPTH; i_lig++) {
				if(this.getResolvedCombinationMatrix().getElement(i_lig, i_col).booleanValue()) {
					if (!frameFirstOccurrenceAlreadyFound) {
						this.getDataManager().getLogger().traceErasureCorrectorDiagonalise("\t\tLower Left part: ");
						frameFirstOccurrenceAlreadyFound = true;
						// Swap(i_lig,i_col);
						this.getDataManager().getLogger().traceErasureCorrectorDiagonalise("Swap("+i_lig+";"+i_col+") ");
						this.getResolvedCombinationMatrix().swapRows(i_lig, i_col);
						this.getResolvedDataMatrix().swapRows(i_lig, i_col);
					}else {
						// Xor(i_lig,i_col);
						this.getDataManager().getLogger().traceErasureCorrectorDiagonalise("Xor("+i_lig+";"+i_col+") ");
						this.getResolvedCombinationMatrix().xorRows(i_lig, i_col);
						this.getResolvedDataMatrix().xorRows(i_lig, i_col);
					}
				}
			}
			if (frameFirstOccurrenceAlreadyFound) {
				this.getDataManager().getLogger().traceErasureCorrectorDiagonalise(" Upper Right part: ");
				for (int i_lig=0; i_lig<i_col; i_lig++) {
					if( this.getResolvedCombinationMatrix().getElement(i_lig, i_col).booleanValue()) {
						// Xor(i_lig,i_col);
						this.getDataManager().getLogger().traceErasureCorrectorDiagonalise("Xor("+i_lig+";"+i_col+") ");
						this.getResolvedCombinationMatrix().xorRows(i_lig, i_col);	
						this.getResolvedDataMatrix().xorRows(i_lig, i_col);

					}
				}
				this.getDataManager().getLogger().traceErasureCorrectorDiagonalise("\n");
			}
		}
	}


	/**
	 * Ins�re le fragment dans la base de donn�e.
	 * Le fragment est simplifi� puis les matrices de donn�es et de comvinaisons sont maj.
	 * @param f le fragement � inserer.
	 */
	private void insert(Fragment f) {
		int FCNT;
		FCNT = f.getCounter();
		this.getDataManager().getLogger().traceErasureCorrector("\t\tRoll from expected : [" + this.computeExpectedFragmentNumber() + "] to received : ["+ FCNT +"]\n");

		this.rollToReicevedFragmentNumber(FCNT);
		this.setLastFragmentNumberTreated(FCNT);

		if ( FCNT < 128 ) { // Cas systematique
			// TODO utiliser LoRaWAN frame counter pour detecter trous
			this.getDataManager().getLogger().traceErasureCorrector("\t\tSystematic fragment. Append to matrix.\n");
			this.getDataManager().getLogger().traceErasureCorrectorLite("Receive systematic:"+f.toString()+"\n");
			this.getDataResolveMatrix().setBeforeLastRow(f.getPayload());
			this.getCombinationResolveMatrix().setBeforeLastElement(Boolean.TRUE);

		} else { // Cas redondant
			this.getDataManager().getLogger().traceErasureCorrector("\t\tRedundant fragment. Extract new information.\n");
			this.getDataManager().getLogger().traceErasureCorrectorLite("Receive Redundant:"+f.toString()+"\n");

			Pair<Integer[],Boolean[]> simplifiedFragment;
			simplifiedFragment = this.simplifyReceivedFragment(f);
			for (int i=0; i<simplifiedFragment.getValue().length ;i++) {
				if	(simplifiedFragment.getValue()[i].booleanValue()) {
					this.getDataResolveMatrix().setRow(2*i+1, simplifiedFragment.getKey());
					this.getCombinationResolveMatrix().setRow(2*i+1, simplifiedFragment.getValue());
					break;
				}
			}
		}
	}

	private void rollToReicevedFragmentNumber(int FCNT) {
		do { // On fait glisser les matrices de decodage.
			this.setExpectedFragmentNumber(this.computeExpectedFragmentNumber());
			if (this.getExpectedFragmentNumber()<128) { //Fragment systematique
				this.getCombinationResolveMatrix().shiftUp();
				this.getDataResolveMatrix().shiftUp();
				this.getFragmentsDeliveryBuffer().shift();
				this.eraseCorruptedLines();
				this.getCombinationResolveMatrix().shiftLeft();
				this.getCombinationResolveMatrix().shiftUp(); // On fait remonter 2x avec les systematique et 0 x avec les  redundants car trame de la forme [SSSSSRRR] et nno plus [SRSRSRSR]
				this.getDataResolveMatrix().shiftUp();
			}
			if(FCNT!=this.getExpectedFragmentNumber()) {
				this.setLastFragmentNumberTreated(this.getExpectedFragmentNumber());
			}
		}while(FCNT!=this.getExpectedFragmentNumber());
	}

	private Pair<Integer[],Boolean[]> simplifyReceivedFragment(Fragment received) {
		LogicalMatrix RM = this.getCombinationResolveMatrix();
		BytesMatrix DM = this.getDataResolveMatrix();

		Integer[] RDR;
		Boolean[] RCR;
		int fragNum = received.getCounter() - 128;
		RCR = this.computeRandomCombination(fragNum);
		this.getDataManager().getLogger().traceErasureCorrector("\t\tFragment. Extract new information:\n");
		this.getDataManager().getLogger().traceErasureCorrectorLite("\t\t\tReceived combination: [" + Utility.BooleanTableToString(RCR) + "]\n");
		
		//		System.out.println("Combination "+received.getFragmentNumber()+ "[" + Trace.BooleanTableToString(RCR) + "]\n");

		RDR = received.getPayload();

		for (int i=0; i<RCR.length ;i++) {
			if(RCR[i].booleanValue()) {
				if(RM.getElement(2*i, i).booleanValue()) { // Cette trame est d�j� connue en clair
					// Je peux la retirer de la combinaison.
					this.getDataManager().getLogger().traceErasureCorrectorLite("Xor("+this.getDataManager().DECODING_DEPTH*i+";"+i+") ");
					RCR = LogicalMatrix.xor(RCR, RM.getRow(2*i));
					Integer[] dataLig = DM.getRow(2*i);
					RDR = BytesMatrix.xor(RDR, dataLig);							
				}else if (RM.getElement(2*i+1, i).booleanValue()) { //Cette trame est deja recu dans une combinaison
					// On l'elimine de celle la
					this.getDataManager().getLogger().traceErasureCorrectorLite("Xor("+this.getDataManager().DECODING_DEPTH*i+";"+i+") ");
					RCR = LogicalMatrix.xor(RCR, RM.getRow(2*i+1));
					RDR = BytesMatrix.xor(RDR, DM.getRow(2*i+1));					
				}
			}
		}
		this.getDataManager().getLogger().traceErasureCorrectorLite("\n");
		this.getDataManager().getLogger().traceErasureCorrector("\t\t\tNew combination :     [" + Utility.BooleanTableToString(RCR) + "]\n");
		return new Pair<Integer[], Boolean[]>(RDR, RCR);
	}

	/**
	 * Supprime les lignes de combinaisons qui contiennent la trame sur le point d'�tre jet�
	 * car si elles ne pourront plus �tre utile au d�codage 
	 * car tout donn�e sera mix� avec une trame inconnue.
	 */
	private void eraseCorruptedLines() {
		for( int i=0; i<this.getCombinationResolveMatrix().nbRows(); i++) {
			if(this.getCombinationResolveMatrix().getElement(i, 0).booleanValue()) {
				this.getCombinationResolveMatrix().eraseRow(i);
			}
		}

	}

	private boolean endOfSequence() {
		int nbFrag = this.getDataManager().getNumberFragmentsByIntegrityDataUnit();
		int fragNum = this.getLastFragmentNumberTreated() ;
		if (fragNum < 128) { //systematic
			if ((fragNum % nbFrag) == nbFrag-1) {
				return true;
			}else {
				return false;
			}
		} else {
			if (((fragNum -128)% nbFrag) == nbFrag-1) {
				return true;
			}else {
				return false;
			}

		}
	}

	private int computeExpectedFragmentNumber() {
		int fragNum = this.getLastFragmentNumberTreated() ;
		int nbFrag = this.getDataManager().getNumberFragmentsByIntegrityDataUnit();
		int next;
		

		if (fragNum < 128) { //systematic
			if (this.endOfSequence()) { // Le prochain sera la premiere redondance de la sequence
				next = fragNum + 1 - nbFrag + 128 ;
			} else {
				next = fragNum+1;
			}
		} else { //Redundant
			if (this.endOfSequence()) { // Le prochain sera la premiere systematique de la sequence suivante
				next = (fragNum + 1 - 128) % (this.getDataManager().getFragmentNumberMax()+1) ;
			} else {
				next = fragNum +1;
			}	
		}
		return next;
	}

	// Prbs23 qui donne s�quence de 0 et 1 en fonction du numero de sequence.
	public Boolean[] computeRandomCombination(int sequenceNumber) {
		int WL = this.getWindowSize();

		Boolean combination [] = new Boolean [this.getDataManager().DECODING_DEPTH*WL];
		Boolean ML [] = this.matrix_line(sequenceNumber);
		for(int j=0; j<this.getDataManager().DECODING_DEPTH-1;j++) {
			for (int i=0; i<WL*2; i++) {
				combination[(WL*j)+i] = Boolean.FALSE;
			}
		}
		for (int i=0; i<WL; i++) {
			combination[i+WL*(this.getDataManager().DECODING_DEPTH-1)] = ML[i];
		}

		return combination;
	}



	/**
	 * From NS mathlab code
	 * returns the line N containing M (0 or 1s) of the encoding parity check matrix
	 *	 is a vector of 1/0 of size M
	 */
	Boolean[] matrix_line(int N){
		int winSize = this.getWindowSize();
		Boolean[] matrix_line_local = new Boolean[winSize];

		for (int i=0; i<matrix_line_local.length; i++) {
			matrix_line_local[i] = new Boolean(false);
		}

		// m = 2^(floor(log2(M-1))+1); %power of 2 immediately superior to M;
		int m;
		if (Integer.highestOneBit(winSize) == Integer.lowestOneBit(winSize)) { // if M is a power of 2
			m=1;
		}else {
			m=0;
		}

		int x = 1+1001*N;
		int r;
		int nb_coeff=0;

		while (nb_coeff<winSize/2) { // M/2 = taux d'incluion de 50%
			r = 1 << 16; // 2^16
			while (r>=winSize) {
				x=prbs23(x);
				r = x % (winSize+m); // on fait modulo M si M n'et pas une puissnace de 2 , sinon M+1
			}	    
			matrix_line_local[r] = new Boolean(true); //set to 1 the column which were randomly selected
			nb_coeff = nb_coeff + 1;
		}

		return matrix_line_local;

	}

	static private int prbs23(int seed) {
		int x, b0, b1, returnValue;
		x = seed;
		b0 = (x & 1);
		//		b1 = bitand(x,32)/32;
		b1 = (x & 32) >>> 5;
		// x = floor(x/2) + bitxor(b0,b1)*2^22
		x = (x>>>1) + (b1^b0) * (1 << 22); 
		returnValue=x;
		return returnValue;
	}


	/* ******************* *
	 *   GETTERS-SETTERS   *
	 * ******************* */

	private DataUnitDeliveryBuffer getFragmentsDeliveryBuffer() {
		return this.getDataManager().getFragmentsDeliveryBuffer();
	}



	/**	
	 * @return the dataManager
	 */
	private DataManager getDataManager() {
		return dataManager;
	}

	/**
	 * @param dataManager the dataManager to set
	 */
	private void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}

	/**
	 * @return the dataResolveMatrix
	 */
	private BytesMatrix getDataResolveMatrix() {
		return this.getDataManager().getDataResolveMatrix();
	}

	/**
	 * @param dataResolveMatrix the dataResolveMatrix to set
	 */
	private void setDataResolveMatrix(BytesMatrix dataResolveMatrix) {
		this.getDataManager().setDataResolveMatrix(dataResolveMatrix);
	}
	/**
	 * @return the combinationResolveMatrix
	 */
	private LogicalMatrix getCombinationResolveMatrix() {
		return this.getDataManager().getCombinationResolveMatrix();
	}
	/**
	 * @param combinationResolveMatrix the combinationResolveMatrix to set
	 */
	private void setCombinationResolveMatrix(LogicalMatrix combinationResolveMatrix) {
		this.getDataManager().setCombinationResolveMatrix(combinationResolveMatrix);
	}


	/**
	 * @return the lastFragmentNumberTreated
	 */
	private int getLastFragmentNumberTreated() {
		return this.getDataManager().getLastFragmentNumberTreated();
	}
	/**
	 * @param lastFragmentNumberTreated the lastFragmentNumberTreated to set
	 */
	private void setLastFragmentNumberTreated(int lastFragmentNumberTreated) {
		this.getDataManager().setLastFragmentNumberTreated(lastFragmentNumberTreated);
	}
	/**
	 * @return the expectedFragmentNumber
	 */
	private int getExpectedFragmentNumber() {
		return expectedFragmentNumber;
	}

	/**
	 * @return the windowSize
	 */
	private int getWindowSize() {
		return this.getDataManager().getWindowSize();
	}

	/**
	 * @param windowSize the windowSize to set
	 */
	private void setWindowSize(int windowSize) {
		this.getDataManager().setWindowSize(windowSize);
	}

	/**
	 * @return the fragmentLength
	 */
	private int getFragmentLength() {
		return this.getDataManager().getFragmentLength();
	}

	/**
	 * @param fragmentLength the fragmentLength to set
	 */
	private void setFragmentLength(int fragmentLength) {
		this.getDataManager().setFragmentLength(fragmentLength);
	}

	/**
	 * @param expectedFragmentNumber the expectedFragmentNumber to set
	 */
	private void setExpectedFragmentNumber(int expectedFragmentNumber) {
		this.expectedFragmentNumber = expectedFragmentNumber;
	}

	/**
	 * @return the resolvedCombinationMatrix
	 */
	public LogicalMatrix getResolvedCombinationMatrix() {
		return resolvedCombinationMatrix;
	}

	/**
	 * @param resolvedCombinationMatrix the resolvedCombinationMatrix to set
	 */
	private void setResolvedCombinationMatrix(LogicalMatrix resolvedCombinationMatrix) {
		this.resolvedCombinationMatrix = resolvedCombinationMatrix;
	}

	/**
	 * @return the resolvedDataMatrix
	 */
	private BytesMatrix getResolvedDataMatrix() {
		return resolvedDataMatrix;
	}

	/**
	 * @param resolvedDataMatrix the resolvedDataMatrix to set
	 */
	private void setResolvedDataMatrix(BytesMatrix resolvedDataMatrix) {
		this.resolvedDataMatrix = resolvedDataMatrix;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = "[Resolver]\n";
		S += "  ";
		for (int i=0; i<this.getCombinationResolveMatrix().nbColumns();i++) {
			S += String.format("%d", i%10);
		}
		S += " | ";
		for (int i=0; i<this.getDataResolveMatrix().nbColumns();i++) {
			S += String.format("  %d", i%10);
		}
		S += " | ";
		for (int i=0; i<this.getCombinationResolveMatrix().nbColumns();i++) {
			S += String.format("%d", i%10);
		}
		S += "\n";
		for (int i=0; i<this.getCombinationResolveMatrix().nbRows();i++) {
			S += String.format("%d ", i%10);
			for (int j=0; j<this.getCombinationResolveMatrix().nbColumns();j++) {
				S = S + this.getCombinationResolveMatrix().elementToString(i, j);
			}
			S += " | ";
			for (int j=0; j<this.getDataResolveMatrix().nbColumns();j++) {
				S = S + this.getDataResolveMatrix().elementToString(i, j);
			}
			S += " | ";
			for (int j=0; j<this.getResolvedCombinationMatrix().nbColumns();j++) {
				S = S + this.getResolvedCombinationMatrix().elementToString(i, j);
			}
			S += "\n";
		}
		S += this.getFragmentsDeliveryBuffer();
		S += "\t-----\n";
		return S;		
	}




}
