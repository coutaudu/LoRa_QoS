package ENGINE;
import java.util.Arrays;
import java.util.Vector;

import DATA_STRUCTURES.ApplicationDataUnit;
import DATA_STRUCTURES.DataManager;
import DATA_STRUCTURES.Fragment;
import DATA_STRUCTURES.IntegrityDataUnit;
import DATA_STRUCTURES.LoRaWANFrame;
import SERVER.Utility;


/**
 * COUTAUD Ulysse
 * SEMTECH
 * Universite Grenoble Alpes
 * ulysse.coutaud@univ-grenoble-alpes.fr
 * ucoutaud@semtech.com
 */

/**
 * @author coutaudu
 *
 */
public class Decoder {

	/* ******************* *
	 *       TRACE         *
	 * ******************* */


	/* ******************* *
	 *     CONSTANTS       *
	 * ******************* */

	public static final int APPLICATION_COUNTER_LSB_SIZE = 1;
	public static final int HASH_SIZE = 2;
	public static final int INTEGRITY_PROTOCOL_OVERHEAD = APPLICATION_COUNTER_LSB_SIZE + HASH_SIZE ;

	public static final int FRAGMENTS_COUNTER_SIZE	= 1;
	public static final int FEC_PROTOCOL_OVERHEAD = FRAGMENTS_COUNTER_SIZE;

	/* ******************* *
	 *     ATTRIBUTES      *
	 * ******************* */


	private DataManager dataManager;
	private ErasureCorrector resolver;
	private Defragmenter defragmenter;
	private Integrity integrity;

	/* ******************* *
	 *     CONSTRUCTOR     *
	 * ******************* */

	public Decoder(int windowSize, int applicationDataUnitLength) {
		if(1>=windowSize) {throw new RuntimeException("windowSize must be > 1.");}
		if(0>=applicationDataUnitLength) {throw new RuntimeException("applicationDataUnitLength must be > 0.");}

		// Init le dataManager (les structures de donnees)
		this.setMyData(new DataManager());
		this.setWindowSize(windowSize);
		this.setApplicationLayerDataUnitLength(applicationDataUnitLength);
		this.setIntegrityLayerDataUnitLength(applicationDataUnitLength + INTEGRITY_PROTOCOL_OVERHEAD);
		this.initFragmentLength();
//		System.out.println("FL="+this.getFragmentLength());
		this.getDataManager().setNumberFragmentsByIntegrityDataUnit(this.computeNbFragmentsByDataUnit());
//		System.out.println("nb Frag by data units:"+this.computeNbFragmentsByDataUnit());
		//		System.out.println("nbFrag="+this.getMyData().getNumberFragmentsByIntegrityDataUnit());
		int nbFragMax = 128 - (128 % this.getDataManager().getNumberFragmentsByIntegrityDataUnit()); //rq c'est le nombre de fragment mais les indices commencent � 0.
		this.getDataManager().setFragmentNumberMax(nbFragMax-1); 
		int max = this.getDataManager().getFragmentNumberMax()+128;
		this.setFragmentCounter(max);
		int padding = (this.getDataManager().getNumberFragmentsByIntegrityDataUnit()*this.getDataManager().getFragmentLength()-this.getDataManager().getIntegrityLayerDataUnitLength());
		this.getDataManager().setPadding(padding);
		this.getDataManager().initBuffers();


		// Init le Resolver
		this.setResolver(new ErasureCorrector(this.getDataManager()));
		this.setDefragmenter(new Defragmenter(this.getDataManager(), computeNbFragmentsByDataUnit() ));
		this.setIntegrity(new Integrity(this.getDataManager()));
	}

	/* ******************* *
	 *       METHODS       *
	 * ******************* */

	public Vector<ApplicationDataUnit> receiveFrame(LoRaWANFrame receivedFrame) {
		Vector<IntegrityDataUnit> reconstructedIntegrityDataUnits;
		IntegrityDataUnit currentIntegrityDataUnit;
		Vector<ApplicationDataUnit> validReceivedApplicationDataUnits = new Vector<ApplicationDataUnit>();

		this.getDataManager().getLogger().trace("\n#-RCV-#" + " "+receivedFrame + "\n");


		/* *** PARSE la trame brute recue *** */
		int nbFragAppFrame = this.getDataManager().getNumberFragmentsByIntegrityDataUnit();
		int fragMax=this.getDataManager().getFragmentNumberMax();
		int fragLen = this.getFragmentLength();
		Fragment[] rcvFragments = Fragment.parse(receivedFrame.payload, fragLen, fragMax, nbFragAppFrame);

		//		for (int i=0; i< rcvFragments.length; i++) {
		//			System.out.println(rcvFragments[i]);
		//		}
		for (int i_frag_rcv=0; i_frag_rcv<rcvFragments.length; i_frag_rcv++) {
			this.getDataManager().getLogger().traceLite("\n#==============================================================================#\n");
			/* ******************************** */
			/* *** FORWARD ERROR CORRECTION *** */
			/* ******************************** */
			this.getDataManager().getLogger().traceErasureCorrector("\t\t|*-*-*-*-*-*-RESOLVER-*-*-*-*-*-*|\nPush Fragment: " + rcvFragments[i_frag_rcv].toString() + "\n");
			this.getResolver().pushFragment(rcvFragments[i_frag_rcv]);
			this.getDataManager().getLogger().traceErasureCorrectorDataManagerDetails("\nDataManager state:\n"+ this.getDataManager().toString());
			this.getDataManager().getLogger().traceErasureCorrector("\t\t|*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-|\n" );
			boolean doDefrag = rcvFragments[i_frag_rcv].getCounter() >128 
					|| rcvFragments[i_frag_rcv].getCounter()%this.getDataManager().getNumberFragmentsByIntegrityDataUnit() == this.getDataManager().getNumberFragmentsByIntegrityDataUnit()-1;
//			System.out.println(doDefrag);
//			System.out.println("numrelatif:"+rcvFragments[i_frag_rcv].getCounter()%this.getDataManager().getNumberFragmentsByIntegrityDataUnit());

			if(doDefrag) {
				/* *********************** */
				/* *** DEFRAGMENTATION *** */
				/* *********************** */
				this.getDataManager().getLogger().traceDefragmenter("\t\t|*-*-*-*-*-*-DEFRAGMENTER-*-*-*-*-*-*|\nDefragmentation:\n" + this.getDataManager().getFragmentsDeliveryBuffer()+ "\n");
				reconstructedIntegrityDataUnits = this.getDefragmenter().pull();
				this.getDataManager().getLogger().traceDefragmenter("\tReconstructed Integrity Data Units:\n");
				for (int i=0; i<reconstructedIntegrityDataUnits.size(); i++) {
					this.getDataManager().getLogger().traceDefragmenter("\t\t["+reconstructedIntegrityDataUnits.get(i).toString()+ "]\n");
				}

				/* ***************** */
				/* *** INTEGRITY *** */
				/* ***************** */
				this.getDataManager().getLogger().traceIntegrity("\t\t|*-*-*-*-*-*-INTEGRITY-*-*-*-*-*-*|\n");
				for (int i=0; i<reconstructedIntegrityDataUnits.size(); i++) {
					currentIntegrityDataUnit = reconstructedIntegrityDataUnits.get(i);
					/* *** INTEGRITY *** */
					this.getDataManager().getLogger().traceIntegrity("\tCheck integrity of :" + currentIntegrityDataUnit +"\n");
					if ( this.getIntegrity().isValid(currentIntegrityDataUnit) ) {
						currentIntegrityDataUnit.getPayload();
						ApplicationDataUnit ADU = new ApplicationDataUnit(currentIntegrityDataUnit);
						validReceivedApplicationDataUnits.add(ADU);
					}else {
						// Wrong hash 
						// TODO Detect First Potential Error Frame
						// TODO IL faut reset comme si on attend le numero de fragment du premier fragment de la trame phyqieu recu couramment
						int previous =  Fragment.computePrevious(rcvFragments[0].getCounter(), this.getDataManager().getFragmentNumberMax(), this.getDataManager().getNumberFragmentsByIntegrityDataUnit());
						this.getDataManager().reset(previous);
						this.getDataManager().getLogger().trace("\n\t---ERROR HASH DETECTED---\n\t   Reset at ["+previous+"]");

						return null;
					}
				}

			}
		}
		for (int i=0; i<validReceivedApplicationDataUnits.size(); i++) {
			this.getDataManager().getLogger().trace("\n"+validReceivedApplicationDataUnits.elementAt(i).toString());
		}
		return validReceivedApplicationDataUnits;
	}


	// Privates
	private void initFragmentLength() {
		int computedFragmentLength; // Taille de la payload contenu dans chaque fragment. Cad sans le fragmentCounter, donc maximum 9 pour respecter US reglementation � SF12.
		int integrityDataUnitLength ;  // Taille de la trame de la couche d'integrit�, cad la payload applicative + le lsb du compteur applicatif + 2 octets de MIC.
		integrityDataUnitLength = this.getIntegrityLayerDataUnitLength();
		double nbFragmentsByIntegrityDataUnits = Math.ceil(integrityDataUnitLength/9.0); // A = Nb de fragments necessaire pour transporter la payload
		computedFragmentLength = (int) (Math.ceil((double)integrityDataUnitLength / nbFragmentsByIntegrityDataUnits));
		this.setFragmentLength(computedFragmentLength);
	}

	private int computeNbFragmentsByDataUnit() {
		int CDUL = this.getIntegrityLayerDataUnitLength();
		int FL = this.getFragmentLength();
		int retval = (int)Math.ceil((double)CDUL/(double)FL);
		return retval;
	}













	/****************************************************************************************/
	/* ******************* *
	 *   GETTERS-SETTERS   *
	 * ******************* */


	/**
	 * @return the windowSize
	 */
	private int getWindowSize() {
		return this.getDataManager().getWindowSize();
	}

	/**
	 * @param windowSize the windowSize to set
	 */
	private void setWindowSize(int windowSize) {
		this.getDataManager().setWindowSize(windowSize);
	}



	/**
	 * @return the fragmentLength
	 */
	private int getFragmentLength() {
		return this.getDataManager().getFragmentLength();
	}

	/**
	 * @param fragmentLength the fragmentLength to set
	 */
	private void setFragmentLength(int fragmentLength) {
		this.getDataManager().setFragmentLength(fragmentLength);
	}

	/**
	 * @return the applicationLayerDataUnitLength
	 */
	private int getApplicationLayerDataUnitLength() {
		return this.getDataManager().getApplicationLayerDataUnitLength();
	}

	/**
	 * @param applicationLayerDataUnitLength the applicationLayerDataUnitLength to set
	 */
	private void setApplicationLayerDataUnitLength(int applicationLayerDataUnitLength) {
		this.getDataManager().setApplicationLayerDataUnitLength(applicationLayerDataUnitLength);
	}

	/**
	 * @return the integrityLayerDataUnitLength
	 */
	private int getIntegrityLayerDataUnitLength() {
		return this.getDataManager().getIntegrityLayerDataUnitLength();
	}

	/**
	 * @param integrityLayerDataUnitLength the integrityLayerDataUnitLength to set
	 */
	private void setIntegrityLayerDataUnitLength(int integrityLayerDataUnitLength) {
		this.getDataManager().setIntegrityLayerDataUnitLength(integrityLayerDataUnitLength);
	}



	/**
	 * @return the fragmentCounter
	 */
	private int getFragmentCounter() {
		return this.getDataManager().getLastFragmentNumberTreated();
	}

	/**
	 * @param fragmentCounter the fragmentCounter to set
	 */
	private void setFragmentCounter(int fragmentCounter) {
		this.getDataManager().setLastFragmentNumberTreated(fragmentCounter);
	}

	/**
	 * @return the myData
	 */
	public DataManager getDataManager() {
		return dataManager;
	}

	/**
	 * @param myData the myData to set
	 */
	private void setMyData(DataManager myData) {
		this.dataManager = myData;
	}

	/**
	 * @return the resolver
	 */
	public ErasureCorrector getResolver() {
		return resolver;
	}

	/**
	 * @param resolver the resolver to set
	 */
	public void setResolver(ErasureCorrector resolver) {
		this.resolver = resolver;
	}




	/**
	 * @return the defragmenter
	 */
	public Defragmenter getDefragmenter() {
		return defragmenter;
	}

	/**
	 * @param defragmenter the defragmenter to set
	 */
	public void setDefragmenter(Defragmenter defragmenter) {
		this.defragmenter = defragmenter;
	}




	/**
	 * @return the integrity
	 */
	public Integrity getIntegrity() {
		return integrity;
	}

	/**
	 * @param integrity the integrity to set
	 */
	public void setIntegrity(Integrity integrity) {
		this.integrity = integrity;
	}

	public void setDefragmenterTrace(boolean b) {
		this.getDataManager().getLogger().setTRACE_DEFRAGMENTER(b);
	}

	public void setResolverTrace(boolean b) {
		this.getDataManager().getLogger().setTRACE_RESOLVER(b);
	}

	public void setIntegrityTrace(boolean b) {
		this.getDataManager().getLogger().setTRACE_INTEGRITY(b);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Decoder" 
				+ "\n\t["+ this.getWindowSize() 						+ "] windowSize"  
				+ "\n\t["+ this.getApplicationLayerDataUnitLength() 	+ "] applicationLayerDataUnitLength"
				+ "\n\t["+ this.getIntegrityLayerDataUnitLength() 	+ "] integrityLayerDataUnitLength"
				+ "\n\t["+ this.getFragmentLength()					+ "] fragmentLength"
				+ "\n\t["+ this.getFragmentCounter() 					+ "] fragmentCounter"
				+ "\n"+ dataManager 							+ " myData\n" ;
	}











}
