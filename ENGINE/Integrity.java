package ENGINE;

import java.util.Arrays;

import DATA_STRUCTURES.DataManager;
import DATA_STRUCTURES.IntegrityDataUnit;

//import com.sun.corba.se.impl.ior.ByteBuffer;

/**
 * @author ucoutaud
 *
 */
public class Integrity {
	private DataManager dataManager;

	public Integrity(DataManager dataManager) {
		this.setDataManager(dataManager);
	}




	public boolean isValid(IntegrityDataUnit frame) {
		int MSB = this.getDataManager().getApplicationFramesCounterMostSignificantsBytes();
		this.getDataManager().getLogger().traceIntegrity("\t\tCurrent application counter Most Significants Bytes: [" + MSB + "]\n");
		frame.computeApplicationFrameCounter(MSB);
//		this.getDataManager().getLogger().traceErasureCorrectorLite(">Try frame >>> "+frame.toString());
		if (checkHash(frame)) {
			this.getDataManager().getLogger().traceIntegrity("\t\tHash valid. Applicative frame can be delivered.");
			return true;
		}else {
			for (int j=1; j<100; j++) { //TODO BORNER avec differences sur le frame_counter lorawan pour reperer trou.
				this.getDataManager().getLogger().traceIntegrity("\t\tHash invalid. Check with MSB["+(MSB+j)+"]\n");
				frame.computeApplicationFrameCounter(MSB+j);
				if (checkHash(frame)) {
					this.getDataManager().getLogger().traceIntegrity("\t\tHash valid. Applicative frame can be delivered.]\n");
					// TODO detecter et decider increment du data manager APP_COUNT -> quand numero de fragment recu correspond � on a abandonn� les fragments du MSB courant
					if (j >= 2) {
						this.getDataManager().setApplicationFramesCounterMostSignificantsBytes(MSB+j-1);
					}
					return true;
				}
			}
		}

		return false;
	}


	private static boolean checkHash(IntegrityDataUnit frame) {
		byte[] src = new byte[frame.getPayload().length];
		for (int i=0; i<frame.getPayload().length;i++) {
			src[i] = frame.getPayload()[i].byteValue();
		}
//		System.out.println("src["+Arrays.toString(src)+"]");
		byte [] computedHash = AesCmac.computeHash(frame.getApplicationFrameCounter(), src);
//		System.out.println("Computed HASH["+Arrays.toString(computedHash)+"]");
		if (frame.getHashBytesArray()[0] == computedHash[0] && frame.getHashBytesArray()[1] == computedHash[1] ) {
			return true;
		}
		return false;
	}

	/**
	 * @return the dataManager
	 */
	public DataManager getDataManager() {
		return dataManager;
	}

	/**
	 * @param dataManager the dataManager to set
	 */
	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}



}
