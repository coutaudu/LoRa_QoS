package ENGINE;
import java.awt.List;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Vector;
import java.util.function.DoubleToLongFunction;

import DATA_STRUCTURES.DataManager;
import DATA_STRUCTURES.IntegrityDataUnit;

/**
 * COUTAUD Ulysse
 * SEMTECH
 * Universite Grenoble Alpes
 * ulysse.coutaud@univ-grenoble-alpes.fr
 * ucoutaud@semtech.com
 */

/**
 * @author ucoutaud
 *
 */
public class Defragmenter {
	
	private DataManager dataManager;
	private int numberFragmentsByDataUnit;
	
	public Defragmenter(DataManager dataManager, int nbFragmentsByDataUnit) {
		this.setDataManager(dataManager);
		this.setNumberFragmentsByDataUnit(nbFragmentsByDataUnit);
	}

	public Vector<IntegrityDataUnit> pull() {
		int numberFramesInBuffer;
		Vector<IntegrityDataUnit> defragmentedIntegrityDataUnit;
		
		numberFramesInBuffer = Math.floorDiv(this.getBufferLength(), this.getNumberFragmentsByDataUnit());
		defragmentedIntegrityDataUnit = new Vector<IntegrityDataUnit>(); 
		for (int i=0; i<numberFramesInBuffer; i++) {
			this.getDataManager().getLogger().traceDefragmenter(" Try frame[" + i + "] ");
			if(this.allFragmentRecovered(i)) {
				this.getDataManager().getLogger().traceDefragmenter("\n\tAll Fragments Recovered[" + i + "]\n");
				defragmentedIntegrityDataUnit.add(this.reconstructFrame(i));
			}
		}
		return defragmentedIntegrityDataUnit;
	}

	private  IntegrityDataUnit reconstructFrame(int i) {
		Integer[][] constitutiveFragments = new Integer[this.getNumberFragmentsByDataUnit()][this.getDataManager().getFragmentLength()];;
		for(int j=0; j<this.getNumberFragmentsByDataUnit(); j++) {
			int index = ((this.getDataManager().getWindowSize()*this.getDataManager().DECODING_DEPTH-1)-(i)*this.getNumberFragmentsByDataUnit())-j;
			constitutiveFragments[this.getNumberFragmentsByDataUnit()-(j+1)] = this.getFragmentsDeliveryBuffer().getResolvedDataUnits().getRow(index);
			this.setFrameReconstituted(index);
		}
		int padding =this.getDataManager().getPadding();
		return new IntegrityDataUnit(constitutiveFragments, padding); //TODO check valeur du padding;
	}


	private boolean allFragmentRecovered(int i) {
		int count = 0;
		for(int j=0; j<this.getNumberFragmentsByDataUnit(); j++) {
			int index = ((this.getDataManager().getWindowSize()*this.getDataManager().DECODING_DEPTH-1)-(i)*this.getNumberFragmentsByDataUnit())-j;
			boolean fragmentIsCorrected = false;
			fragmentIsCorrected = this.getDataManager().getFragmentsDeliveryBuffer().getStatusDeliveredFromPreviousLayer().getElement(index, 0) 
					&& !this.getDataManager().getFragmentsDeliveryBuffer().getStatusDeliveredToNextLayer().getElement(index, 0);
			if (fragmentIsCorrected ) {
				this.getDataManager().getLogger().traceDefragmenter("\t\tFragment[" + j + "] Recovered ("+index+")\n");
				String S = "";
				for (int k=0; k<this.getDataManager().getFragmentsDeliveryBuffer().getResolvedDataUnits().nbColumns(); k++) 
					S+=this.getDataManager().getFragmentsDeliveryBuffer().getResolvedDataUnits().elementToString(index, k);
				this.getDataManager().getLogger().traceDefragmenter(S);
					count++;
			}
		}
		return count==this.getNumberFragmentsByDataUnit();
	}

	private Integer[] getFragment(int i) {
		return this.getFragmentsDeliveryBuffer().getResolvedDataUnits().getRow(i);
	}
	
	private void setFrameReconstituted(int i) {
		this.getFragmentsDeliveryBuffer().getStatusDeliveredToNextLayer().setElement(i, 0, Boolean.TRUE);
	}
	
	private boolean frameReconstituted(int i) {
		return this.getFragmentsDeliveryBuffer().getStatusDeliveredToNextLayer().getElement(i, 0);
	}
	
	private boolean isFragmentReady(int i) {
		return this.getFragmentsDeliveryBuffer().getStatusDeliveredFromPreviousLayer().getElement(i, 0);
	}
	
	private DataUnitDeliveryBuffer getFragmentsDeliveryBuffer() {
		return this.getDataManager().getFragmentsDeliveryBuffer();
	}
	
	private int getBufferLength() {
		return this.getFragmentsDeliveryBuffer().getBufferLength();
	}
	
	
	/**
	 * @return the dataManager
	 */
	public DataManager getDataManager() {
		return this.dataManager;
	}

	/**
	 * @param dataManager the dataManager to set
	 */
	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}

	/**
	 * @return the nbFragmentsByDataUnit
	 */
	public int getNumberFragmentsByDataUnit() {
		return numberFragmentsByDataUnit;
	}

	/**
	 * @param nbFragmentsByDataUnit the nbFragmentsByDataUnit to set
	 */
	public void setNumberFragmentsByDataUnit(int nbFragmentsByDataUnit) {
		this.numberFragmentsByDataUnit = nbFragmentsByDataUnit;
	}



	

}
