package DATA_STRUCTURES;
/**
 * 
 */

/**
 * Unit� de donn�e de niveau applicatif
 * Compos� d'un identifiant (num�ro de trame) et de la donn�e applicative (pas de restriction de taille � priori)
 * @author ucoutaud
 */
public class ApplicationDataUnit {

	private Integer applicationFrameCounter;
	private byte[] payload;
	

	/**
	 * Constructeur
	 * @param IDU La Integrity Data Unit source.
	 * La fonction parse et filtre IDU pour en extraire l'Application Data Unit.
	 * Remarque: Ne v�rifie pas la valeur du HASH de IDU, doit �tre fait en amont.
	 */
	public ApplicationDataUnit(IntegrityDataUnit IDU	) {
		this.setApplicationFrameCounter(new Integer(IDU.getApplicationFrameCounter().intValue()));
		this.setPayload(new byte[IDU.getPayload().length]);
		for (int i=0; i<this.getPayload().length; i++) {
			this.getPayload()[i] = IDU.getPayload()[i].byteValue();
		}
	}
	/**
	 * @return the applicationFrameCounter
	 */
	public Integer getApplicationFrameCounter() {
		return applicationFrameCounter;
	}
	/**
	 * @param applicationFrameCounter the applicationFrameCounter to set
	 */
	private void setApplicationFrameCounter(Integer applicationFrameCounter) {
		this.applicationFrameCounter = applicationFrameCounter;
	}
	/**
	 * @return the payload
	 */
	public byte[] getPayload() {
		return payload;
	}
	/**
	 * @param payload the payload to set
	 */
	private void setPayload(byte[] payload) {
		this.payload = payload;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = "";
		S += String.format("~%d~", this.getApplicationFrameCounter());
		for (int i=0; i<this.getPayload().length; i++) {
			S += String.format(" %3X", this.getPayload()[i]);
		}

		return S;
	}
	
}
