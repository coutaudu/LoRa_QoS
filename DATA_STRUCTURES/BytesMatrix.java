package DATA_STRUCTURES;
import java.util.Arrays;

/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class BytesMatrix extends CircularMatrix<Integer>  {


	public BytesMatrix( int height, int width) {

		super(height, width);

		this.data = new Integer[height][width];
		for(int i=0;i<this.data.length;i++) {
			for(int j=0; j<this.data[0].length;j++) {
				this.setElement(i, j,  0); 
			}
		}
	}


	@Override
	public String elementToString(int row, int column) {
		if(row>=this.nbRows) {throw new RuntimeException("BytesMatrix elementToString dimension mismatch (arg row exceed nbRows).");}
		if(column>=this.nbColumns) {throw new RuntimeException("BytesMatrix elementToString dimension mismatch (arg column exceed nbColumns).");}

		String S = String.format("%3X", this.getElement(row, column).byteValue());
		return S;
	}

	@Override
	public Integer nullElement() {
		return 0;
	}

	public static Integer[] xor(Integer[] A, Integer[] B) {
		if (A.length <= 0 || B.length<=0) {throw new RuntimeException("Error error xor segment of different size;");}

		Integer[] retval = new Integer[A.length];
		for(int i=0; i<A.length; i++) {
			retval[i] = A[i] ^ B[i];
		}
		return retval;
	}

	public void swapRows(int rowA, int rowB) {
		if(rowA>=this.nbRows) {throw new RuntimeException("BytesMatrix swapRows dimension mismatch (arg rowA exceed nbRows).");}
		if(rowB>=this.nbRows) {throw new RuntimeException("BytesMatrix swapRows dimension mismatch (arg rowB exceed nbRows).");}

		Integer buf = new Integer(0);
		for (int i=0; i<this.nbColumns; i++) {
			buf = this.getElement(rowA, i);
			this.setElement(rowA, i, this.getElement(rowB, i));
			this.setElement(rowB, i, buf);
		}
	}


	public void xorRows(int rowA, int rowB) {
		if(rowA>=this.nbRows) {throw new RuntimeException("BytesMatrix xorRows dimension mismatch (arg rowA exceed nbRows).");}
		if(rowB>=this.nbRows) {throw new RuntimeException("BytesMatrix xorRows dimension mismatch (arg rowB exceed nbRows).");}

		Integer buf = new Integer(0);
		for (int i=0; i<this.nbColumns; i++) {
			buf = this.getElement(rowA, i) ^ this.getElement(rowB, i);
			this.setElement(rowA, i, buf);
		}
	}

	protected Integer[][] copyData(){
		Integer[][] retval = new Integer[this.nbRows][this.nbColumns];
		for (int i=0; i<this.nbRows; i++) {
			for (int j=0; j<this.nbColumns; j++) {
				retval[i][j] = new Integer(this.data[i][j]);
			}
		}
		return retval;
	}

	@Override
	public Integer[] getRow(int row){
		if(row>=this.nbRows) {throw new RuntimeException("BytesMatrix getRow dimension mismatch (arg row exceed nbRows).");}

		Integer[] retval = new Integer[this.nbColumns];
		for (int i=0; i<this.nbColumns; i++) {
			retval[i] = this.getElement(row, i);
		}
		return retval;
	}

}
