package DATA_STRUCTURES;
import java.nio.ByteBuffer;
import java.nio.channels.NetworkChannel;
import java.util.Arrays;
import java.util.Vector;

/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class IntegrityDataUnit {

	private Integer applicationFrameCounterLeastSignificantByte;
	private Integer applicationFrameCounter;
	private Integer[] payload;
	private Integer hash;
	/**
	 * 
	 */
	public IntegrityDataUnit(Integer [][] fragments, int padding) {
		Vector<Integer> vectorBuffer = new Vector<Integer>();
		for (int i=0; i<fragments.length; i++) {
			for (int j=0; j<fragments[i].length; j++) {
				vectorBuffer.add(fragments[i][j]);
			}
		}
		this.setApplicationFrameCounterLeastSignificantByte(vectorBuffer.get(0));
		ByteBuffer bytesBufferHash = java.nio.ByteBuffer.allocate(2);
		bytesBufferHash.put(vectorBuffer.get(vectorBuffer.size()-(2+padding)).byteValue());
		bytesBufferHash.put(vectorBuffer.get(vectorBuffer.size()-(1+padding)).byteValue());
		bytesBufferHash.flip();
		this.setHash((int) bytesBufferHash.getShort());
		this.payload = new Integer[vectorBuffer.size()-(3+padding)];
		for (int i=1; i<vectorBuffer.size()-(2+padding); i++) {
			this.payload[i-1] = vectorBuffer.get(i);
		}
	}

	public void computeApplicationFrameCounter(int MSB) {
//		int MSB = this.getDataManager().getApplicationFramesCounterMostSignificantsBytes();
		MSB = MSB << 8;
		int LSB = this.getApplicationFrameCounterLeastSignificantByte();
		int retval = MSB | LSB;
		this.setApplicationFrameCounter(retval);
	}

	public byte[] getApplicationFramesCounterAndPayloadBytesArray() {
		java.nio.ByteBuffer byteBuffer = java.nio.ByteBuffer.allocate(4+this.getPayload().length);
		byteBuffer.putInt(this.getApplicationFrameCounter());
		for (int i=0; i<this.getPayload().length; i++) {
			byteBuffer.put(this.getPayload()[i].byteValue());
		}
		byte[] retval = byteBuffer.array();
		return retval;
	}

	public byte[] getHashBytesArray() {
		java.nio.ByteBuffer byteBuffer = java.nio.ByteBuffer.allocate(2);
		byteBuffer.putShort(this.getHash().shortValue());
		// TODO --> V�rifier par rapport au fait que c'est  sur 2 octets en vrai;
		byte[] retval = byteBuffer.array();
		return retval;
	}



	
	/**
	 * @return the applicationFrameCounter
	 */
	public Integer getApplicationFrameCounter() {
		return applicationFrameCounter;
	}


	/**
	 * @param applicationFrameCounter the applicationFrameCounter to set
	 */
	public void setApplicationFrameCounter(Integer applicationFrameCounter) {
		this.applicationFrameCounter = applicationFrameCounter;
	}


	/**
	 * @return the applicationFrameCounter
	 */
	public Integer getApplicationFrameCounterLeastSignificantByte() {
		return applicationFrameCounterLeastSignificantByte;
	}
	/**
	 * @param applicationFrameCounter the applicationFrameCounter to set
	 */
	private void setApplicationFrameCounterLeastSignificantByte(Integer applicationFrameCounter) {
		this.applicationFrameCounterLeastSignificantByte = applicationFrameCounter;
	}
	/**
	 * @return the payload
	 */
	public Integer[] getPayload() {
		return payload;
	}
	/**
	 * @param payload the payload to set
	 */
	private void setPayload(Integer[] payload) {
		this.payload = payload;
	}
	/**
	 * @return the hash
	 */
	public Integer getHash() {
		return hash;
	}
	/**
	 * @param hash the hash to set
	 */
	private void setHash(Integer hash) {
		this.hash = hash;
	}
	
	
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = "";
		S += String.format("~%d~", this.getApplicationFrameCounterLeastSignificantByte());
		for (int i=0; i<this.getPayload().length; i++) {
			S += String.format(" %3X", this.getPayload()[i]);
		}
		S += String.format("| %4X", this.getHash().shortValue());

		return S;
	}
	
	
	
	
}


