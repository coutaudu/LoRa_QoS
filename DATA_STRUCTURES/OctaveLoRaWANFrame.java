package DATA_STRUCTURES;

import java.util.Arrays;

import SERVER.Utility;

public class OctaveLoRaWANFrame {

	public String deviceID;
	public Integer frameCounter;
	public Integer[] payload;
	
	
	/**
	 * Parse raw received byte from lorawan frame.
	 * Abstraction d'une trame LoRaWAN.
	 * Contient les informations extraites de la trame LoRaWAN brute.
	 * Les donn�es sont reshaper pour notre utilisation.
	 * @param deviceID
	 * @param frameCounter
	 * @param loraQosDataUnit
	 */
	public OctaveLoRaWANFrame(String loraqosDeviceIdentifier, Integer lorawanFrameCounter, Integer[] loraqosDataUnit) {

		this.deviceID = loraqosDeviceIdentifier;
		this.frameCounter = lorawanFrameCounter;
		this.payload = loraqosDataUnit;
	
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "-" + deviceID + "#" + frameCounter + "-[" + Arrays.toString(payload) + "]";
	}
	
	
	
}
