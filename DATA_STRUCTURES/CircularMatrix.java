package DATA_STRUCTURES;


/**
 * @author ucoutaud
 * @param <T>
 * Matrice circulaire.
 * Acc�s aux donn�es est relatif aux rotations de la matrice.
 */

public abstract class CircularMatrix<T>  {

	// La table des donn�es brutes
	protected T data [][];
	// Indices des rotations
	protected int indexFirstRow, indexFirstColumn;
	// Dimensions de la matrice
	protected int nbRows, nbColumns;

	
	/**
	 * Constructeur de la matrice circulaire.
	 * L�ve runtime exception si dimensions n�gatives ou nulles.
	 * @param height
	 * @param width
	 */
	public CircularMatrix( int height, int width) {
		if (height <= 0 || width<=0) {throw new RuntimeException("Error empty Matrix;");}
		this.nbRows = height;
		this.nbColumns = width;
		this.indexFirstRow = 0;
		this.indexFirstColumn = 0;
		this.data = null;
	}

	
	/**
	 * @return Une copie des donn�es brutes.
	 */
	protected abstract T[][] copyData();
	
	public void copy(CircularMatrix<T> destination){
		destination.indexFirstColumn = this.indexFirstColumn;
		destination.indexFirstRow = this.indexFirstRow;
		destination.nbRows = this.nbRows;
		destination.nbColumns = this.nbColumns;
		destination.data = this.copyData();		
	}
	
	/**
	 * @return L'�l�ment neutre du type de donn�es manipul�es.
	 * Ex: 0 pour les octets. Faux pour les booleens.
	 */
	public abstract T nullElement();
	
	/**
	 * @param row le num�ro de la ligne.
	 * @return La ligne de la matrice.
	 */
	public abstract T[] getRow(int row);
	
	/**
	 * rowBuf = rowA
	 * rowA = rowB
	 * rowB = rowBuf
	 * @param rowA
	 * @param rowB
	 */
	public abstract void swapRows(int rowA, int rowB);
	
	/**
	 * rowA = rowA xor rowB
	 * @param rowA
	 * @param rowB
	 */
	public abstract void xorRows(int rowA, int rowB);
	
	
	/**
	 * Efface une ligne de la matrice (met tout ces �l�ments � la valeur neutre du type de donn�es).
	 * @param row l'indice de la ligne � effacer.
	 */
	public void eraseRow(int row){
		if(row>=this.nbRows) {throw new RuntimeException("Matrix eraseRow dimension mismatch (arg row exceed nbRows).");}
		for (int i=0; i < this.data[0].length; i++) {
			this.eraseElement(row, i);
		}
	}
	
	/**
 	 * Efface une colonne de la matrice (met tout ces �l�ments � la valeur neutre du type de donn�es).
	 * @param column l'indice de la colonne � effacer.
	 */
	public void eraseColumn(int column){
		if(column>=this.nbColumns) {throw new RuntimeException("Matrix eraseColumn dimension mismatch (arg column exceed nbColumns).");}

		for (int i=0; i < this.data.length; i++) {
			this.eraseElement(i, column);
		}		
	}
	
	/**
	 * Efface un �l�ment de la matrice (valeur neutre)
	 * @param row indice de la ligne.
	 * @param column indice de la colonne.
	 */
	public void eraseElement(int row, int column) {
		if(row>=this.nbRows) {throw new RuntimeException("Matrix eraseElement dimension mismatch (arg row exceed nbRows).");}
		if(column>=this.nbColumns) {throw new RuntimeException("Matrix eraseElement dimension mismatch (arg column exceed nbColumns).");}
		this.setElement(row, column, this.nullElement());
	}
	
	/**
	 * Affecte la derni�re ligne de la matrice.
	 * @param data
	 */
	public void setBeforeLastRow(T data[]) {
		this.setRow(nbRows-2, data);
	}

	/**
	 * Affecte le dernier �l�ment de la matrice (derni�re ligne, derni�re colonne).
	 * @param value
	 */
	public void setBeforeLastElement(T value) {
		this.setElement(nbRows-2, nbColumns-1, value);
	}

	
	/**
	 * L�ve Runtime Exception si les indices d�borde des dimensions de la matrice.
	 * @param row indice de la ligne.
	 * @param col indice de la colonne.
	 * @return L'�l�ment (row,col) de la matrice.
	 */
	public T getElement(int row, int col) {
		if(row>=this.nbRows) {throw new RuntimeException("Matrix getElement dimension mismatch (arg row exceed nbRows).");}
		if(col>=this.nbColumns) {throw new RuntimeException("Matrix getElement dimension mismatch (arg col exceed nbColumns).");}
		int lig = (row+indexFirstRow)%nbRows;
		int lacol = (col+indexFirstColumn)%nbColumns;
		return this.data[lig][lacol];			
	}

	/**
	 * Affecte l'�l�ment (row,col) de la matrice.
	 * L�ve Runtime Exception si les indices d�borde des dimensions de la matrice.
	 * @param row indice de la ligne.
	 * @param col indice de la colonne.
	 * @param value valeur � affecter
	 */
	public void setElement(int row, int col, T value) {
		if(row>=this.nbRows) {throw new RuntimeException("Matrix setElement dimension mismatch (arg row exceed nbRows).");}
		if(col>=this.nbColumns) {throw new RuntimeException("Matrix setElement dimension mismatch (arg col exceed nbColumns).");}
		this.data[(row+this.indexFirstRow)%nbRows][(col+this.indexFirstColumn)%nbColumns] = value;			
	}

	

	/**
	 * Affecte une ligne de la matrice
	 * @param row indice de la ligne.
	 * @param data donn�es � affecter.
	 */
	public void setRow(int row, T[] data) {
		if(data.length!=this.nbColumns) {throw new RuntimeException("Matrix setRow dimension mismatch (number of columns).");}
		if(row>=this.nbRows) {throw new RuntimeException("Matrix setRow dimension mismatch (arg row exceed nbRows).");}
		for (int j=0; j<this.nbColumns;j++) {
			this.setElement(row, j, data[j]);
		}
	}


	/**
	 * Effectue une rotation de 1 ligne vers le haut.
	 * Toute ligne i est affect� � ligne i+1
	 * La premi�re ligne est effac�e.
	 * La derni�re ligne est affect� � neutre.
	 * 
	 */
	public void shiftUp() {
		this.eraseRow(0);
		this.indexFirstRow = (this.indexFirstRow+1) % this.nbRows; 
	}

	/**
	 * Effectue une rotation de 1 colonne vers la gauche.
	 * Toute colonne i est affect� � colonne i+1.
	 * La premi�re colonne est effac�e.
	 * La derni�re colonne est affect�e � neutre. 
	 */
	public void shiftLeft() {
		this.eraseColumn(0);
		this.indexFirstColumn = (this.indexFirstColumn+1) % this.nbRows; 
	}
	
	public int nbRows() {
		return this.nbRows;
	}
	
	public int nbColumns() {
		return this.nbColumns;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = "   ";
		for (int j=0; j<this.nbColumns;j++) {
			S += j%10;
		}
		S += "\n";

		for (int i=0; i<this.nbRows;i++) {
			S+=i+" ";
			if (i<10) S+= " ";
			for (int j=0; j<this.nbColumns;j++) {
				S = S + this.elementToString(i, j);
			}
			S += "\n";
		}
		return S;
	}

	public abstract	String elementToString(int row, int column);

}




