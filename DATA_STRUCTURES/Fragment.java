package DATA_STRUCTURES;


import ENGINE.Decoder;
import SERVER.Utility;

/**
 * COUTAUD Ulysse
 * SEMTECH
 * Universite Grenoble Alpes
 * ulysse.coutaud@univ-grenoble-alpes.fr
 * ucoutaud@semtech.com
 */

/**
 * @author coutaudu
 * Fragment l'unit� de donn�e de la couche correction d'effacement.
 */
public class Fragment {
	private int counter;
	private Integer payload[];
	
	/* ******************* *
	 *     CONSTRUCTOR     *
	 * ******************* */

	public static int computeNext(int numFragment, int numMax, int nbFragByFrame) {
		int retval;
		if (numFragment < 128) { //systematic
			if ((numFragment % nbFragByFrame) == nbFragByFrame-1) { //endOfSequence
				retval = numFragment + 1 - nbFragByFrame + 128 ;
			}else {  // NOT endOfSequence
				retval = numFragment + 1 ;
			}
		} else { //redundant
			if (((numFragment -128)% nbFragByFrame) == nbFragByFrame-1) {//endOfSequence
				retval =  (numFragment + 1 - 128) % (numMax+1) ;;
			}else { // NOT endOfSequence
				retval = numFragment + 1 ;
			}
		}
		return retval;
	}
	
	//TODO v�rifier
	public static int computePrevious(int numFragment, int numMax, int nbFragByFrame) {
		int retval;
		if (numFragment < 128) { //systematic
			if ((numFragment % nbFragByFrame) == 0) { //beginOfSequence
				retval = (numFragment - 1) % (numMax+1) + 128 ;
			}else {  // NOT beginOfSequence
				retval = numFragment - 1 ;
			}
		} else { //redundant
			if ((numFragment % nbFragByFrame) == 0) {//beginOfSequence
				retval =  (numFragment - 128) + nbFragByFrame-1;
			}else { // NOT beginOfSequence
				retval = numFragment - 1 ;
			}
		}
		return retval;	}
	
	
	public static Fragment[] parse (Integer [] bytes, int fragmentLength, int numberFragmentMax, int nbFragByAppFrame) {
		// Cas 1 unique fragment

		if ( bytes.length == (fragmentLength) + 1) {
//			System.out.println("Fragment unique.");
			Fragment[] retval = new Fragment[1];
			Integer [] frag = new Integer[fragmentLength+1];
			for (int j=0; j<fragmentLength+1; j++) {
				frag[j] = bytes[j];
			}
			retval[0] = new Fragment(frag);
			return retval;
		}
		// Cas multiples fragments
		else if ( bytes.length % (fragmentLength) == 1) {
			
//			System.out.println("Fragment multiple.");
			int numFrag = bytes[0]; 
			int nbFragments = (bytes.length-1) / fragmentLength ; 
			Fragment[] retval = new Fragment[nbFragments];
			int curseur = 1;
			for (int i=0; i<nbFragments; i++) {
				Integer[] frag = new Integer[fragmentLength+1];
				frag[0] = numFrag;
				numFrag = Fragment.computeNext(numFrag, numberFragmentMax, nbFragByAppFrame);
//				System.out.println("Fragment Number:["+frag[0]+"]");
				for (int j=1; j<fragmentLength+1;j++) {
					frag[j] = bytes[curseur];
					curseur ++;
				}
				retval[i] = new Fragment(frag);
				//Compute next fragment number in physical frame
			}
			return retval;
		}else {
			throw new RuntimeException("\nReceived Incoherent number of byte.\n Received ["+bytes.length+"] for fragments length ["+fragmentLength+"]" );
		}
	}
	
	public Fragment(Integer[] bytes) {
		this.setCounter(bytes[0]);
		this.setPayload(bytes);
	}
	
	

	/* ******************* *
	 *   GETTERS-SETTERS   *
	 * ******************* */

	
	/**
	 * @return the fragmentNumber
	 */
	public int getCounter() {
		return counter;
	}


	/**
	 * @param fragmentNumber the fragmentNumber to set
	 */
	@SuppressWarnings("unused")
	private void setCounter(int fragmentNumber) {
		if (Decoder.FRAGMENTS_COUNTER_SIZE > 1) {
			throw new RuntimeException("Not Implented yet. FRAGMENTS_COUNTER_SIZE must be 1;");
		}
		this.counter = fragmentNumber;

	}

	/**
	 * @param bytes the Payload of the fragment
	 * @param fragmentLength length of the fragment in current docder
	 */
	private void setPayload(Integer bytes[]) {
		int fragmentLength = bytes.length-1;
		this.payload = new Integer[fragmentLength];
		System.arraycopy(bytes, 1, this.payload, 0,fragmentLength);
	}


	/**
	 * @return payload
	 */
	public Integer[] getPayload() {
		return this.payload;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S = "";
		for (int i=0; i<this.payload.length;i++) {
			S += String.format("%3X",payload[i]);
		}
		return "-" + counter + "-[" + S + "]";
	}



}
