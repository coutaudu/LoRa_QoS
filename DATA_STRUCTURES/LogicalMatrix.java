package DATA_STRUCTURES;
import java.util.Arrays;

/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class LogicalMatrix extends CircularMatrix<Boolean>{

	public LogicalMatrix( int height, int width) {
		super(height, width);
		this.data = new Boolean[height][width];
		for(int i=0;i<this.data.length;i++) {
			for(int j=0; j<this.data[0].length;j++) {
				this.setElement(i, j,  Boolean.FALSE); 
			}
		}
	}
	
	public LogicalMatrix( int height, int width, boolean value) {
		super(height, width);
		this.data = new Boolean[height][width];
		for(int i=0;i<this.data.length;i++) {
			for(int j=0; j<this.data[0].length;j++) {
				this.setElement(i, j,  new Boolean(value)); 
			}
		}
	}


	@Override
	public String elementToString(int row, int column) {
		if (this.getElement(row, column).booleanValue()) {
			return "1";
		}else {
			return "0";
		}
	}

	@Override
	public Boolean nullElement() {
		return Boolean.FALSE;
	}

	public static Boolean[] xor(Boolean[] A, Boolean[] B) {
		if (A.length <= 0 || B.length<=0) {
			throw new RuntimeException("Error error xor segment of different size;");
		}
		Boolean[] retval = new Boolean[A.length];
		for(int i=0; i<A.length; i++) {
			retval[i] = A[i] ^ B[i];
		}
		return retval;
	}
	
	public void swapRows(int rowA, int rowB) {
		if(rowA>=this.nbRows) {throw new RuntimeException("LogicalMatrix swapRows dimension mismatch (arg rowA exceed nbRows).");}
		if(rowB>=this.nbRows) {throw new RuntimeException("LogicalMatrix swapRows dimension mismatch (arg rowB exceed nbRows).");}

		Boolean buf = new Boolean(false);
		for (int i=0; i<this.nbColumns; i++) {
			buf = this.getElement(rowA, i);
			this.setElement(rowA, i, this.getElement(rowB, i));
			this.setElement(rowB, i, buf);
		}
	}

	

	public void xorRows(int rowA, int rowB) {
		if(rowA>=this.nbRows) {throw new RuntimeException("LogicalMatrix xorRows dimension mismatch (arg rowA exceed nbRows).");}
		if(rowB>=this.nbRows) {throw new RuntimeException("LogicalMatrix xorRows dimension mismatch (arg rowB exceed nbRows).");}

		Boolean buf = new Boolean(false);
		for (int i=0; i<this.nbColumns; i++) {
			buf = this.getElement(rowA, i) ^ this.getElement(rowB, i);
			this.setElement(rowA, i, buf);
		}
	}

	protected Boolean[][] copyData(){
		Boolean[][] retval = new Boolean[this.nbRows][this.nbColumns];
		for (int i=0; i<this.nbRows; i++) {
			for (int j=0; j<this.nbColumns; j++) {
				retval[i][j] = new Boolean(this.data[i][j]);
			}
		}
		return retval;
	}

	public boolean rowWithSingleOne(int row) {
		if(row>=this.nbRows) {throw new RuntimeException("LogicalMatrix rowWithSingleOne dimension mismatch (arg row exceed nbRows).");}

		boolean retval = false;
		for (int i=0; i<this.nbColumns; i++) {
			if (this.getElement(row, i).booleanValue()) {
				if (retval) {
					return false; // Il y a plusieurs TRUE sur la ligne
				}else {
					retval = true; // On a au moins 1 True sur la ligne
				}
			}
		}
		return retval; 
	}
	
	@Override
	public Boolean[] getRow(int row){
int bla =0; 
		if(row>=this.nbRows) {throw new RuntimeException("LogicalMatrix getRow dimension mismatch (arg row exceed nbRows).");}
		Boolean[] retval = new Boolean[this.nbColumns];
		for (int i=0; i<this.nbColumns; i++) {
			retval[i] = this.getElement(row, i);
		}
		return retval;
	}



}
