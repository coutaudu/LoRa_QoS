package DATA_STRUCTURES;

import ENGINE.DataUnitDeliveryBuffer;
import SERVER.Logger;

/**
 * COUTAUD Ulysse
 * SEMTECH
 * Universite Grenoble Alpes
 * ulysse.coutaud@univ-grenoble-alpes.fr
 * ucoutaud@semtech.com
 */

/**
 * @author coutaudu
 * Le branchement avec la Base de Donnee.
 * TODO
 */
public class DataManager {

	/* ******************* *
	 *     ATTRIBUTES      *
	 * ******************* */
	private final int CODING_RATIO = 2;
	public final int DECODING_DEPTH = 2;
	
	// La matrice contenant les donnees combin�es.
	private BytesMatrix dataResolveMatrix;
	// La matrice contenant l'informations sur les combinaisosn de donn�es.
	private LogicalMatrix combinationResolveMatrix;
	// Buffer pour faire passer les donn�es extraites par le decodage (MPC), � la couche de d�fragmentation.
	private DataUnitDeliveryBuffer fragmentsDeliveryBuffer;
		
	// Le num�ro de s�quence du dernier fragment de trait�e. Base pour calculer le fragment attendu.
	private int lastFragmentNumberTreated;

	private int windowSize; // Taille de la fen�tre de correction.
	private int numberOfFragmentsByIntegrityDataUnit; // Nombre de fragments pour constituer une unit� de donn�es applicative.
	private int padding; // Nombre de bytes � remplir pour remplir le dernier fragment lors de la fragmentation.
	private int fragmentNumberMax; // Numero de fragment max (de base 255) afin que malgr� la fragmentation, numFrag=0 corresponde � un d�but d'unit� de donn�es applicative.
	private int fragmentLength; // Nombre d'octets des fragments (<=6 pour rentrer dans la plus petite payload LoRaWan possible= 10 - 1 octets de numero de fragment + 1 octets de numero de unit� de donn�es applicative + 2 octets de hash)
	private int applicationLayerDataUnitLength; // Taille des unit�s de donn�es applicatives (=payload de l'utilisateur + entete de LoRaQoS =num trame appli).
	private int integrityLayerDataUnitLength; // Taille de l'unit� de donn�es de la couche int�grit� = (payload de l'utilisateur + num trame appli, + 2 hash)

	private int applicationFramesCounterMostSignificantsBytes; // M�moire du cot� serveur des octets de poids fort du compteur d'unit� de donn�es applicative (permet de reconstituer le compteur sur 4 octets, sur lequel on calcule le hash).  
	
	private Logger log;

	/* ******************* *
	 *     CONSTRUCTOR     *
	 * ******************* */

	
	public DataManager() {
		this.log = new Logger();
		//void => initialisation des parametres + appel a initBuffers avant utilisation
	}
	

	/**
	 * Affecte m�moire des 3 buffers du data manager.
	 */
	public void initBuffers() {
		this.setFragmentsDeliveryBuffer(new DataUnitDeliveryBuffer(this));
		this.setCombinationResolveMatrix(new LogicalMatrix(DECODING_DEPTH*CODING_RATIO*this.getWindowSize(),DECODING_DEPTH*this.getWindowSize()) );
		this.setDataResolveMatrix(new BytesMatrix(DECODING_DEPTH*CODING_RATIO*this.getWindowSize(),this.getFragmentLength()));
	}

	// TODO doit etre fait pas decodeur et pas datamanager qui est juste le pont avec la DB.
	/**
	 * Initialise les valeurs des buffers.
	 */
	public void reset() {
		this.reset(255);
	}
	
	/**
	 * R�-init les donn�es du d�codeur � la reception d'un fragement donn�es
	 * @param frag
	 */
	//TODO modif avec les fenetres a crans
	public void reset(int frag) {
		this.initBuffers();
		this.setApplicationFramesCounterMostSignificantsBytes(0);
		// TODO /!\ check difference cas redondant ou systematique.
		for (int i=0; i<this.getWindowSize()*DECODING_DEPTH; i++) {
			this.getCombinationResolveMatrix().setElement(i*CODING_RATIO+1, i, Boolean.TRUE);
			this.getCombinationResolveMatrix().setElement(i*CODING_RATIO, i, Boolean.TRUE);
		}
		this.setLastFragmentNumberTreated(frag);

	}
		
	/**
	 * @return the padding
	 */
	public int getPadding() {
		return padding;
	}

	/**
	 * @param padding the padding to set
	 */
	public void setPadding(int padding) {
		this.padding = padding;
	}

	/**
	 * @return the dataResolveMatrix
	 */
	public BytesMatrix getDataResolveMatrix() {
		return dataResolveMatrix;
	}

	/**
	 * @param dataResolveMatrix the dataResolveMatrix to set
	 */
	public void setDataResolveMatrix(BytesMatrix dataResolveMatrix) {
		this.dataResolveMatrix = dataResolveMatrix;
	}

	/**
	 * @return the combinationResolveMatrix
	 */
	public LogicalMatrix getCombinationResolveMatrix() {
		return combinationResolveMatrix;
	}

	/**
	 * @param combinationResolveMatrix the combinationResolveMatrix to set
	 */
	public void setCombinationResolveMatrix(LogicalMatrix combinationResolveMatrix) {
		this.combinationResolveMatrix = combinationResolveMatrix;
	}

	
	/**
	 * @return the fragmentsDeliveryBuffer
	 */
	public DataUnitDeliveryBuffer getFragmentsDeliveryBuffer() {
		return fragmentsDeliveryBuffer;
	}


	/**
	 * @param fragmentsDeliveryBuffer the fragmentsDeliveryBuffer to set
	 */
	public void setFragmentsDeliveryBuffer(DataUnitDeliveryBuffer fragmentsDeliveryBuffer) {
		this.fragmentsDeliveryBuffer = fragmentsDeliveryBuffer;
	}

	
	/**
	 * @return the lastFragmentNumberTreated
	 */
	public int getLastFragmentNumberTreated() {
		return lastFragmentNumberTreated;
	}

	/**
	 * @param lastFragmentNumberTreated the lastFragmentNumberTreated to set
	 */
	public void setLastFragmentNumberTreated(int lastFragmentNumberTreated) {
		this.lastFragmentNumberTreated = lastFragmentNumberTreated;
	}


	/**
	 * @return the windowSize
	 */
	public int getWindowSize() {
		return windowSize;
	}

	/**
	 * @param windowSize the windowSize to set
	 */
	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}

	/**
	 * @return the fragmentLength
	 */
	public int getFragmentLength() {
		return fragmentLength;
	}

	/**
	 * @param fragmentLength the fragmentLength to set
	 */
	public void setFragmentLength(int fragmentLength) {
		this.fragmentLength = fragmentLength;
	}


	/**
	 * @return the applicationLayerDataUnitLength
	 */
	public int getApplicationLayerDataUnitLength() {
		return applicationLayerDataUnitLength;
	}


	/**
	 * @param applicationLayerDataUnitLength the applicationLayerDataUnitLength to set
	 */
	public void setApplicationLayerDataUnitLength(int applicationLayerDataUnitLength) {
		this.applicationLayerDataUnitLength = applicationLayerDataUnitLength;
	}

	
	/**
	 * @return the integrityLayerDataUnitLength
	 */
	public int getIntegrityLayerDataUnitLength() {
		return integrityLayerDataUnitLength;
	}


	/**
	 * @param integrityLayerDataUnitLength the integrityLayerDataUnitLength to set
	 */
	public void setIntegrityLayerDataUnitLength(int integrityLayerDataUnitLength) {
		this.integrityLayerDataUnitLength = integrityLayerDataUnitLength;
	}

	/**
	 * @return the applicationFramesCounter
	 */
	public int getApplicationFramesCounterMostSignificantsBytes() {
		return applicationFramesCounterMostSignificantsBytes;
	}

	/**
	 * @param applicationFramesCounter the applicationFramesCounter to set
	 */
	public void setApplicationFramesCounterMostSignificantsBytes(int applicationFramesCounter) {
		this.applicationFramesCounterMostSignificantsBytes = applicationFramesCounter;
	}
	
	/**
	 * @return the numberFragmentsByDataUnit
	 */
	public int getNumberFragmentsByIntegrityDataUnit() {
		return numberOfFragmentsByIntegrityDataUnit;
	}

	/**
	 * @param numberFragmentsByDataUnit the numberFragmentsByDataUnit to set
	 */
	public void setNumberFragmentsByIntegrityDataUnit(int numberFragmentsByDataUnit) {
		this.numberOfFragmentsByIntegrityDataUnit = numberFragmentsByDataUnit;
	}

	
	
	/**
	 * @return the fragmentNumberMax
	 */
	public int getFragmentNumberMax() {
		return fragmentNumberMax;
	}
	
	public Logger getLogger() {
		return this.log;
	}

	/**
	 * @param fragmentNumberMax the fragmentNumberMax to set
	 */
	public void setFragmentNumberMax(int fragmentNumberMax) {
		this.fragmentNumberMax = fragmentNumberMax;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String S ="[DataManager"
				+ "(WL=" + windowSize
				+"; lastFragRcv=" + this.getLastFragmentNumberTreated()
				+ "; numFragMax=" + this.getFragmentNumberMax()
				+ "; fragLength=" + this.getFragmentLength()
				+ "; nbFragByIntegrityDataUnit=" + this.getNumberFragmentsByIntegrityDataUnit()
				+ "; padding=" + this.getPadding()
				+")]\n";
		S += " ";
		
		for (int i=0; i<this.getCombinationResolveMatrix().nbColumns;i++) {
			S += String.format(" %2d", i);
		}
		S += "\n";
		for (int i=0; i<this.getCombinationResolveMatrix().nbRows;i++) {
			S += String.format("%2d ", i);
//			if (i<10) S+= " ";
			for (int j=0; j<this.getCombinationResolveMatrix().nbColumns;j++) {
				S = S + this.getCombinationResolveMatrix().elementToString(i, j)+ "  ";
			}
			S += " | ";
			for (int j=0; j<this.getDataResolveMatrix().nbColumns;j++) {
				S = S + this.getDataResolveMatrix().elementToString(i, j);
			}
			S += "\n";
		}
		S += "\nBuffer Fragments Delivery:\n";
		S += this.getFragmentsDeliveryBuffer().toString();
		S += "\t-----\n";
		return S;
	}

}
