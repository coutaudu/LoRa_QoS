package Tests_Demos;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import java.util.Vector;

import DATA_STRUCTURES.ApplicationDataUnit;
import DATA_STRUCTURES.LoRaWANFrame;
import END_DEVICE.DeviceConfiguration;
import END_DEVICE.DeviceSimulator;
import ENGINE.Decoder;
import SERVER.FRONT_END_SERVER;
import SERVER.Utility;
import SERVER.WorkerMultipleParityCheck;




public class ServerMultipleParityCheckSimu implements Runnable {


	int port;

	double PER;

	int nbFramesSend;
	boolean log;
	boolean trace;

	DeviceConfiguration deviceConfiguration;
	WorkerMultipleParityCheck workerMPC;
	FRONT_END_SERVER frontEndServer;

	public ServerMultipleParityCheckSimu(int APL, int WL, String id, int nbFramesSend, int port, boolean log, boolean trace){

		this.nbFramesSend = nbFramesSend;
		this.port = port;	
		this.log=log;
		this.trace=trace;
		
		deviceConfiguration = new DeviceConfiguration(APL, WL, id);
		this.workerMPC = new WorkerMultipleParityCheck(deviceConfiguration, trace);
		this.frontEndServer = new FRONT_END_SERVER(port);
		Thread frontEndServerThread = new Thread(frontEndServer);
		frontEndServerThread.start();
		
	}



	@Override
	public void run() {
		double result_PDR;
		try {		
			//LOG
			FileWriter fileWriter = null;
			PrintWriter printWriter = null;
			if (log) {
				fileWriter = new FileWriter("test_"
						+ workerMPC.configuration.APL 
						+ "_"+workerMPC.configuration.WL+"_"
						+ nbFramesSend+".csv"
						, true);
				printWriter = new PrintWriter(fileWriter, true);
				printWriter.printf("  %d_%d_%d\n", workerMPC.configuration.WL, workerMPC.configuration.APL, nbFramesSend);
			}
			
			
			for(int i_per=0; i_per<100; i_per++) {
				if (this.trace) System.out.println("Start " + (double)i_per/100.0);
				this.PER = (double)i_per/100.0;
				result_PDR = this.exec() ;
				if (this.log) printWriter.printf("%f, %f\n", PER, result_PDR);

			}
			if (this.log)  printWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public double exec() throws IOException {
		double retval;
		
		
		if(trace) System.out.println("\t\tEndDeviceSim Start["+"APL"+workerMPC.configuration.APL+"][WL"+workerMPC.configuration.WL+"][port]"+port+"nbFrames:"+this.nbFramesSend);
		DeviceSimulator DS = new DeviceSimulator(workerMPC.configuration, port, this.nbFramesSend, this.PER);

		if(trace) System.out.println("\t\tMPC Worker Start[APL"+workerMPC.configuration.APL+"][WL"+workerMPC.configuration.WL+"]");
		frontEndServer.pushWorker(null, workerMPC, deviceConfiguration.ID);

		retval = (double) this.workerMPC.exec() / (double) this.nbFramesSend;
		if (trace) {
			if (retval==-1) {
				System.out.println("Error Server MPC.");
			}else {
				System.out.println("\t\tMPC Server END [APL"+workerMPC.configuration.APL+"][WL"+workerMPC.configuration.WL+"]");
			}
		}
		return retval;
	}





}








