package Tests_Demos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.*;

import DATA_STRUCTURES.LoRaWANFrame;
import SERVER.FRONT_END_SERVER;

public class serveItJSON {

	static int i=0;

	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
		server.createContext("/test", new MyHandler());
		server.setExecutor(null); // creates a default executor
		server.start();
	}

	static class MyHandler implements HttpHandler {
		@Override
		public void handle(HttpExchange t) throws IOException {
			String LoRaQoSDevID;
			Integer LoRaWANCounter;
			String S = getStringFromInputStream(t.getRequestBody());
			String httpPost = S;

			LoRaWANFrame frm = FRONT_END_SERVER.parseReceivedTextJSON(S);
			System.out.println(frm);
			// TODO
//				LoRaWANFrame rcvFrame = new LoRaWANFrame(bytes);
			
			String response = "My response is HELLOWORD !";
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();

		}
	}

	// convert InputStream to String
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append('\n');
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}