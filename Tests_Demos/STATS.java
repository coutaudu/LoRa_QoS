package Tests_Demos;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Vector;

import SERVER.Utility;

//import com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverFragment;


/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class STATS {



	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args)  {

		int nbFramesSend = 50;
		int port = 5000;

		boolean log=true;
		boolean trace=false;

		int i_id = 0;
		
		Vector<Thread> threads = new Vector<Thread>(); 

		for (int i_WL=8;i_WL<=8; i_WL+=8) {
			for (int i_apl=1; i_apl<2; i_apl+=10) {
				System.out.println("Start Stats: APL "+i_apl+" bytes (no fragmentation) WL "+i_WL+" port "+port+"\n");
				i_id++;
				ServerMultipleParityCheckSimu SMPC = new ServerMultipleParityCheckSimu(i_apl,i_WL, Integer.toString(i_id), nbFramesSend, port, log, trace );
				Thread T = new Thread( SMPC);
						
				port+=2;
				threads.add(T);
				T.start();
			}
		}
		System.out.println("All ["+threads.size()+"] threads started.");
		for (int i=0; i<threads.size(); i++) {
			try {
				threads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(i + " finished.");
		}
		
	}


	
	


}
