package Tests_Demos;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import org.json.simple.*;

//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class postItJSON {

	public static void main(String[] args) throws Exception {
		URL url = new URL("http://localhost:8000/test");
		URLConnection con = url.openConnection();
		HttpURLConnection http = (HttpURLConnection)con;
		http.setRequestMethod("POST"); // PUT is another valid option
		http.setDoOutput(true);

		Map<String,String> arguments = new HashMap<>();
		byte[] dataB = new byte[10];
		dataB[0] = 1;
		dataB[1] = 0;
		dataB[2] = 0;
		dataB[3] = 1;
		dataB[4] = 12;
		dataB[5] = 10;
		dataB[6] = 33;
		dataB[7] = 22;
		dataB[8] = 111;
		dataB[9] = -50;

		byte[] out ;
		JSONObject jsonObj = JsonEncodeDemo(dataB);
		System.out.println(jsonObj.toString());
		out = jsonObj.toString().getBytes();
		int length = out.length;

		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();

		try(OutputStream os = http.getOutputStream()) {
			for(int i=0; i<out.length;i++) System.out.print(out[i]);
			os.write(out);
		}
		http.disconnect();
		
		JsonEncodeDemo(dataB);
	}


	private static JSONObject JsonEncodeDemo(byte[] dataB) {

		JSONObject obj = new JSONObject();
		System.out.println(Integer.toHexString(10));
		String S = new String();
		for (byte b : dataB) {
			S += String.format("0x%02X", b);
		}
		obj.put("Payload", S);
		
//		System.out.println(obj);
		
		return obj;
	}
}

