package Tests_Demos;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Vector;

import SERVER.Utility;

//import com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverFragment;


/**
 * 
 */

/**
 * @author ucoutaud
 *
 */
public class DEMO {



	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args)  {

		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		Thread threadPull[] = new Thread[10];
		int indexThread = 0;
		int APL = 6;
		int WL = 8;
		double PER = 0.2;
		int nbFramesSend = 300;
		int port = 7000;

		boolean log=false;
		boolean trace=true;

		try {
			System.out.println("Start Demo:\n APL 6 bytes (no fragmentation)\n WL 8\n PER 20%");
			Utility.kbstroke();
			ServerMultipleParityCheckSimu SMPC = new ServerMultipleParityCheckSimu(APL, WL, Integer.toString(1), nbFramesSend, port, log, trace);
			SMPC.PER = PER;
			SMPC.exec();

			Utility.kbstroke();

			APL = 20;
			WL=16;
			PER=0.3;
			System.out.println("Start Demo:\n APL 20 bytes (3 fragments)\n WL 16\n PER 30%");
			Utility.kbstroke();
			SMPC = new ServerMultipleParityCheckSimu(APL, WL, Integer.toString(1), nbFramesSend, port+2, log, trace);
			SMPC.PER = PER;
			SMPC.exec();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}






}
