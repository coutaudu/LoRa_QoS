package Tests_Demos;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import org.json.simple.*;

public class postItPOST {

	public static void main(String[] args) throws Exception {
		URL url = new URL("http://localhost:20000/testMPC");
		URLConnection con = url.openConnection();
		HttpURLConnection http = (HttpURLConnection)con;
		http.setRequestMethod("POST"); // PUT is another valid option
		http.setDoOutput(true);

		Map<String,String> arguments = new HashMap<>();
		byte[] dataB = new byte[6];
		dataB[0] = '0';
		dataB[1] = '2';
		dataB[2] = '0';
		dataB[3] = 'A';
		dataB[4] = '1';
		dataB[5] = '0';
//		dataB[6] = 33;
//		dataB[7] = 22;
//		dataB[8] = 111;
//		dataB[9] = -50;

		String DataS = new String(dataB);

		arguments.put("msg=", DataS);

		StringJoiner sj = new StringJoiner("&");
		for(Map.Entry<String,String> entry : arguments.entrySet()) {
			sj.add(entry.getKey()+entry.getValue());
		}
		byte[] out = sj.toString().getBytes();//.getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		http.connect();
		
		try(OutputStream os = http.getOutputStream()) {
//			for(int i=0; i<out.length;i++) System.out.print(out[i]);
			os.write(out);
		}
		http.disconnect();
	}


}

