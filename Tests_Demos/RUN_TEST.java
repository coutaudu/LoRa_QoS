package Tests_Demos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;


import END_DEVICE.DeviceConfiguration;
import SERVER.FRONT_END_SERVER;
import SERVER.WorkerMultipleParityCheck;
import SERVER.FRONT_END_SERVER.RunningDecoder;

public class RUN_TEST {




	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException {

		// Démarre le serveur qui reçoit les données depuis le réseaux.
		FRONT_END_SERVER frontEndServer = new FRONT_END_SERVER(8000);
		UDPtoHTTPadaptor udptohttp = new UDPtoHTTPadaptor(8000, "http://localhost:8000/test");
		Thread frontEndServerThread = new Thread(frontEndServer);
		Thread udptohttpThread = new Thread(udptohttp);
		TriggerNodeRed TNR = new TriggerNodeRed();
		frontEndServerThread.start();
		udptohttpThread.start();

		DeviceConfiguration DC = null; 

		int nbErrorH;
		int numTest = 1;

		File dir = new File ("./src/END_DEVICE/TEST_FLOW/dataTest");
		File dst = new File ("./src/END_DEVICE/TEST_FLOW/nodeRedSrc.input");
		File[] strs = dir.listFiles();

		for (int i = 0; i < strs.length; i++) {
			System.out.println("["+(i+1)+"/"+strs.length+"]");
			Files.copy(strs[i].toPath(), dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
//			System.out.println ("\t["+i+"] "+ strs[i]);
			String[] s = strs[i].getName().split("-");
			
			String ID = "201_"+s[1];
			int APL = Integer.parseInt(s[3]);
			int WL = Integer.parseInt(s[5]);
			System.out.println("Start "+strs[i]);
			DC = new DeviceConfiguration(APL, WL, ID);
			launchDecoder(DC, frontEndServer.runningDecoders);
			TNR.Post("http://localhost:20000/autoTests");
			frontEndServer.runningDecoders.get(0).thread.join();
			nbErrorH = frontEndServer.runningDecoders.get(0).workerMultipleParityCheck.nbErrorHash;
			monitor(frontEndServer.runningDecoders);
			if(nbErrorH==0) {
				System.out.print(" SUCCESS\n");
			}else {
				throw new RuntimeException("\t["+ID+";"+APL+";"+WL+"]"+numTest+"] ERROR");
			}
		}

		return;

	}





	private static String runningDecoderToString(RunningDecoder RD) {
		WorkerMultipleParityCheck SMPC = RD.workerMultipleParityCheck;
		String status = null;
		boolean isAlive = RD.thread.isAlive();
		if (isAlive) {
			status = "Running";
		}else {
			status = "Finished";
		}

		String StringPDR = String.format("%.2f", (SMPC.PDR));
		String StringPER = String.format("%.2f", (SMPC.PER));

		return "Decoder[APL="+SMPC.configuration.APL
				+";WL="+SMPC.configuration.WL
				+"; DevID="+SMPC.configuration.ID
				+"]\n Rcv:"+ SMPC.nbDataUnitDecoded 
				+ " PDR:" + StringPDR  
				+ " PER:" + StringPER
				+ " Reset:" + SMPC.nbErrorHash
				+ " --- "  + status ;
	}

	private static void monitor(LinkedList<RunningDecoder> runningDecoders) {
		for (int i=0; i<runningDecoders.size(); i++) {
			System.out.print(runningDecoderToString(runningDecoders.get(i)));
		}
		for (int i =0; i<runningDecoders.size(); i++) {
			boolean isAlive = runningDecoders.get(i).thread.isAlive();
			if (!isAlive) {
				runningDecoders.remove(i);
				i=0;
			}
		}		
	}


	private static void launchDecoder(DeviceConfiguration dC, LinkedList<RunningDecoder> runningDecoders) {
		if (dC != null) {
			WorkerMultipleParityCheck S = new WorkerMultipleParityCheck(dC, false);
			Thread T = new Thread( S );
			RunningDecoder RD = new RunningDecoder(T, S, dC.ID);
			runningDecoders.add(RD);
			T.start();
		}		
	}


}
