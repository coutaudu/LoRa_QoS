package Tests_Demos;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.*;

import DATA_STRUCTURES.LoRaWANFrame;

public class UDPtoHTTPadaptor implements Runnable{
	int portUDP;
	String urlHTTP;

	public UDPtoHTTPadaptor(int portUDP, String urlHTTP) {
		super();
		this.portUDP = portUDP;
		this.urlHTTP = urlHTTP;
	}

	@Override
	public void run() {

		try {	
			//UDP in
			DatagramSocket dsocket = new DatagramSocket(this.portUDP);
			byte[] bufferUDP = new byte[2048];
			DatagramPacket packet = new DatagramPacket(bufferUDP, bufferUDP.length);

			while(true) {
				dsocket.receive(packet);

				JSONObject jsonObj = JsonEncode(bufferUDP,packet.getLength());
				byte [] out = jsonObj.toString().getBytes();
				int length = out.length;
				URL url = new URL(this.urlHTTP);
				URLConnection con = url.openConnection();
				HttpURLConnection http = (HttpURLConnection)con;
				http.setRequestMethod("POST"); // PUT is another valid option
				http.setDoOutput(true);
				http.setFixedLengthStreamingMode(length);
				http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				http.connect();
				try(OutputStream os = http.getOutputStream()) {
					os.write(out);
				}
				packet.setLength(bufferUDP.length);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	

	private static JSONObject JsonEncode(byte[] dataB, int size) {

		JSONObject obj = new JSONObject();
		String S = new String();
		for (int i=0; i<size; i++) {
			S += String.format("0x%02X", dataB[i]);
		}
		obj.put("Payload", S);

		//		System.out.println(obj);

		return obj;
	}

}

