package Tests_Demos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Arrays;

import com.sun.net.httpserver.*;

public class serveItPOST {

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "This is the response";
            String S;
            
//            System.out.println(t.getRequestURI().toString());
//            
            S = getStringFromInputStream(t.getRequestBody());
//          
//            System.out.println(t.getRequestURI().toString()
//            		+" --- requestBody:["+S + "]"
//            		+t.getRequestMethod()
//            		+"["+t.getRequestURI().getQuery()+"]");
            String [] httpPost = new String[2];
            httpPost = S.toString().split("=");
              
            System.out.println("Parsing:");
            for  (int i=0; i<2; i++) {
            	
            	System.out.println("["+i+"]"+ Arrays.toString(httpPost[i].getBytes()));// new String(httpPost[i].getBytes()),"UTF-8");
            }
            
            System.out.println("Brut:");

            byte[] rcvData = S.getBytes();
            System.out.print("\n[");
            for (int i=0; i<rcvData.length; i++) {
            	System.out.print("." + rcvData[i]);
            }
            System.out.print("]\n");

            
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

 // convert InputStream to String
 	public static String getStringFromInputStream(InputStream is) {

 		BufferedReader br = null;
 		StringBuilder sb = new StringBuilder();

 		String line;
 		try {

 			br = new BufferedReader(new InputStreamReader(is));
 			while ((line = br.readLine()) != null) {
 				sb.append(line);
 				sb.append('\n');
 			}

 		} catch (IOException e) {
 			e.printStackTrace();
 		} finally {
 			if (br != null) {
 				try {
 					br.close();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
 			}
 		}

 		return sb.toString();

 	}
    
}