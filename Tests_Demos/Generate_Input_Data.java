package Tests_Demos;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import END_DEVICE.DeviceConfiguration;

public class Generate_Input_Data {

	public Generate_Input_Data() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException, IOException {

		if (args.length<1) {
			System.out.println("Usage: Generate_Input_Data fileConfig");
		}
		
		Date d = new Date();
		List<String> listConfigurationsTests = Files.readAllLines(Paths.get(args[0]));
		Vector<Integer> APL = new Vector<Integer>();
		Vector<Integer> WL = new Vector<Integer>();
		Vector<String> devID = new Vector<String>();		

		int timeXP = 10;
		String ID = "no_effect";
		DeviceConfiguration devParam;
		String[] parsedConfigurationLine;

		for (int i=0; i<listConfigurationsTests.size(); i++) {
			String line = (String) listConfigurationsTests.get(i);
			if(!line.startsWith("#")) {
				if (line.startsWith("Device")) {
					parsedConfigurationLine  = line.split(" ");
					if (parsedConfigurationLine.length==2) {
						devID.add(parsedConfigurationLine[1]);
					}
				}
				else if (line.startsWith("TimeXP")) {
					parsedConfigurationLine  = line.split(" ");
					if (parsedConfigurationLine.length==2) {
						timeXP=Integer.parseInt(parsedConfigurationLine[1]);
					}
				}
				else if (line.startsWith("XP")) {
					parsedConfigurationLine  = line.split(" ");
					if (parsedConfigurationLine.length==3) {
						APL.add(Integer.parseInt(parsedConfigurationLine[1]));
						WL.add(Integer.parseInt(parsedConfigurationLine[2]));
					}	
				}
			}
		}

		System.out.println("Script start.\n"
				+ d.toString()
				+ "\nConfiguration file: "+args[0]
				+ "\nWill run:"
				+ "\n[APL|WL|Time]");
		int totalTime=0;
		for (int i=0; i< APL.size(); i++) {
			System.out.println("["+APL.get(i)+"|"+WL.get(i)+"|"+timeXP+"]");
			totalTime+=timeXP;			
		}
		long sleepTimeMs = 1000 * 60 * timeXP;

		String basePath = "src"+File.separator+"END_DEVICE"+File.separator+"TEST_FLOW"+File.separator+"nodered"+File.separator;
		System.out.println("Write Data to : "+ basePath);
		System.out.println("Devices: ");
		for (int i=0; i<devID.size(); i++) {
			System.out.println(devID.get(i));
		}
		System.out.println("Duration: " + totalTime + "min (="+totalTime/60+"hours="+(totalTime/60)/24+"days)");

		int i_configuration = 0;
		while (i_configuration<APL.size()) {
			for(int j_device=0; j_device<devID.size();j_device++) {
				devParam = new DeviceConfiguration(APL.get(i_configuration), WL.get(i_configuration), ID);
				devParam.PostConfigurationToDevice(0, devID.get(j_device));
				String marker = ">>>>>>>BEGIN["+APL.get(i_configuration)+","+WL.get(i_configuration)+"]<<<<<<<\n";
				System.out.println(marker);
				Files.write(Paths.get(basePath+devID.get(j_device)+".output"), marker.getBytes(), StandardOpenOption.APPEND);
				i_configuration++;
			}
			d = new Date();
			System.out.println(d.toString()+"\nSleep for "+timeXP+" minutes.");
			Thread.sleep(sleepTimeMs);
		}
		
	}

}
